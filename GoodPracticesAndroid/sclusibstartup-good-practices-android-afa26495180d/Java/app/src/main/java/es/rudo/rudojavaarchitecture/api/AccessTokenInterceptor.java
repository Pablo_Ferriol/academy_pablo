package es.rudo.rudojavaarchitecture.api;

import androidx.annotation.NonNull;

import java.io.IOException;

import es.rudo.rudojavaarchitecture.App;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AccessTokenInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        String accessToken = App.preferences.getAccessToken();
        Request request = newRequestWithAccessToken(chain.request(), accessToken);
        return chain.proceed(request);
    }

    @NonNull
    private Request newRequestWithAccessToken(@NonNull Request request, @NonNull String accessToken) {
        return request.newBuilder()
                .header(Config.TYPE_ITEM_AUTHORIZATION, Config.HTTP_CLIENT_AUTHORIZATION + accessToken)
                .build();
    }
}