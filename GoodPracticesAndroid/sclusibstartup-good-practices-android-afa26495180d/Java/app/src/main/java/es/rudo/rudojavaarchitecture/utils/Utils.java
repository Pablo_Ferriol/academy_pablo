package es.rudo.rudojavaarchitecture.utils;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;


public class Utils {

    public static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public static String[] PERMISSIONS_LOCATION = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    public static int REQUEST_EXTERNAL_STORAGE = 1001;
    private static final int REQUEST_PERMISSION_SETTING = 1020;
    private static final int REQUEST_PERMISSION_LOCATION = 1021;
    public static final String DATE_PATTERN_API = "yyyy-MM-dd HH:mm:ss";

    /**
     * Get String from a Html text
     */

    public static Spanned fromHtml(String text) {
        Spanned textSpanned;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textSpanned = Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT);
            Log.d("STRINGSPANNED", textSpanned.toString());
        } else {
            textSpanned = Html.fromHtml(text);
        }
        return textSpanned;
    }

    public static void configToolbar(AppCompatActivity context, Toolbar toolbar) {
        context.setSupportActionBar(toolbar);
        context.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context.getSupportActionBar().setDisplayShowHomeEnabled(true);
        context.getSupportActionBar().setDisplayShowTitleEnabled(false);
//        toolbar.getNavigationIcon().setColorFilter(context.getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);
//        final Drawable customArrow = context.getResources().getDrawable(R.drawable.ic_arrow_left);
//        context.getSupportActionBar().setHomeAsUpIndicator(customArrow);
    }


    /**
     * Transform string api date to a formatted string
     */

    public static String getFormatDate(String pattern, String dateString) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_PATTERN_API, Locale.getDefault());
        String formatted = "";

        try {

            Date date = simpleDateFormat.parse(dateString);
            simpleDateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
            formatted = simpleDateFormat.format(date);

        } catch (ParseException e) {
            simpleDateFormat = new SimpleDateFormat(DATE_PATTERN_API, Locale.getDefault());
            formatted = "";

            try {

                Date date = simpleDateFormat.parse(dateString);
                simpleDateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
                formatted = simpleDateFormat.format(date);

            } catch (ParseException e1) {
                e1.printStackTrace();
            }

        }

        return formatted;
    }

    public static int[] stringToTimeArray(String timeString, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());

        Calendar calendar = new GregorianCalendar();
        int[] result;
        try {
            calendar.setTime(sdf.parse(timeString));

            result = new int[]{
                    calendar.get(Calendar.HOUR_OF_DAY),
                    calendar.get(Calendar.MINUTE),
                    calendar.get(Calendar.SECOND)
            };
        } catch (Exception e) {
            e.printStackTrace();
            result = null;
        }
        return result;
    }

    /**
     * Load image into imageView with picasso library
     */

    public static void loadGlide(@NonNull Context context, @NonNull String url, @NonNull ImageView view) {
        Activity activity = (Activity) context;
        if (!activity.isFinishing())
            Glide.with(context)
                    .load(url)
                    .into(view);
    }

    public static void loadGlideFit(@NonNull Context context, @NonNull String url, @NonNull ImageView view) {
        Activity activity = (Activity) context;
        if (!activity.isFinishing())
            Glide.with(context)
                    .load(url)
                    .apply(new RequestOptions().fitCenter())
                    .into(view);
    }

    public static void loadGlideCrop(@NonNull Context context, @NonNull String url, @NonNull ImageView view) {
        Activity activity = (Activity) context;
        if (!activity.isFinishing())
            Glide.with(context)
                    .load(url)
                    .apply(new RequestOptions().centerCrop())
                    .into(view);
    }

    public static String getStringFromDate(String pattern, Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
        return simpleDateFormat.format(date);
    }

    public static Date getDateFromString(String pattern, String stringDate) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            Date date = format.parse(stringDate);
            System.out.println(date);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ProgressDialog showProgressDialog(Context context, String message) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);

        return progressDialog;
    }

    public Dialog openDialog(Context context, int layout) {
        Dialog dialog = new Dialog(context);
        dialog.setContentView(layout);

        return dialog;
    }

//    public static Dialog progressDialog(Context context) {
//        Dialog progressDialog;
//        progressDialog = new Dialog(context);
//        progressDialog.setContentView(R.layout.progress_dialog);
//        ProgressBar progressBar = (ProgressBar) progressDialog.findViewById(R.id.progress_bar);
//        progressBar.setVisibility(View.VISIBLE);
//        progressDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
//        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        progressDialog.setCanceledOnTouchOutside(false);
//        progressDialog.setCancelable(false);
//        return progressDialog;
//    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

}

