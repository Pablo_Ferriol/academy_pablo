package es.rudo.kotlinofficialgoodpractices

import es.rudo.kotlinofficialgoodpractices.helpers.Calculator
import org.hamcrest.CoreMatchers.`is` as Is
import org.hamcrest.CoreMatchers.*
import org.hamcrest.Matchers.closeTo
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * @author Voro Ferrer Peris
 *
 * This class implements test methods for class [Calculator].
 * This class also is to be used for reference in future projects.
 * This is a compilation of methods seen at referenced links.
 */
/*
 * Reference links
 * https://developer.android.com/codelabs/android-training-unit-tests#0
 *
 */

class CalculatorTest {
    private var mCalculator: Calculator? = null

    /**
     * Set up the environment for testing
     */
    @Before
    fun setUp() {
        mCalculator = Calculator()
    }

    /**
     * Test for simple addition
     */
    @Test
    fun addTwoNumbers() {
        val resultAdd = mCalculator?.add(1.0, 1.0)
        println("addTwoNumbers $resultAdd")
        assertThat(resultAdd, Is(equalTo(2.0)))
    }

    /**
     * Test for addition with negative numbers
     */
    @Test
    fun addTwoNegativeNumbers() {
        val resultAdd = mCalculator?.add(-1.0, -1.0)
        println("addTwoNegativeNumbers $resultAdd")
        assertThat(resultAdd, Is(equalTo(-2.0)))
    }

    /**
     * Test for addition with mixed numbers
     */
    @Test
    fun addTwoMixedNumbers() {
        var resultAdd = mCalculator?.add(1.0, -1.0)
        println("addTwoMixedNumbers $resultAdd")
        assertThat(resultAdd, Is(equalTo(0.0)))
        resultAdd = mCalculator?.add(-2.0, 1.0)
        println("addTwoMixedNumbers $resultAdd")
        assertThat(resultAdd, Is(equalTo(-1.0)))
    }

    /**
     * Test for addition with Float numbers.
     * Float numbers are not precise so instead of using equalTo() we use closeTo() to indicate an error margin.
     */
    @Test
    fun addTwoFloatNumbers() {
        val resultAdd = mCalculator?.add(1.1111f.toDouble(), 1.111f.toDouble())
        println("addTwoFloatNumbers $resultAdd")
        assertThat(resultAdd, Is(closeTo(2.222f.toDouble(), 0.01)))
    }

    /**
     * Test for simple subtraction.
     */
    @Test
    fun subtractTwoNumbers() {
        val resultSub = mCalculator?.sub(1.0, 1.0)
        println("substractTwoNumbers $resultSub")
        assertThat(resultSub, Is(equalTo(0.0)))
    }

    /**
     * Test for subtraction with negative numbers
     */
    @Test
    fun subtractTwoNegativeNumbers() {
        val resultsSub = mCalculator?.sub(-1.0, -2.0)
        println("substractTwoNegativeNumber $resultsSub")
        assertThat(resultsSub, Is(equalTo(1.0)))
    }


}