package es.rudo.kotlinofficialgoodpractices.helpers

import android.content.Context
import androidx.biometric.BiometricManager

object Utils {

    fun hasBiometrics(context: Context): Int {
        val biometricManager = BiometricManager.from(context)
        return when (biometricManager.canAuthenticate()) {
            BiometricManager.BIOMETRIC_SUCCESS -> Constants.BIOMETRIC_AVAILABLE
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> Constants.BIOMETRIC_UNAVAILABLE
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> Constants.BIOMETRIC_UNAVAILABLE
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> Constants.BIOMETRIC_NOT_REGISTERED
            else -> Constants.BIOMETRIC_UNAVAILABLE
        }
    }

}