package es.rudo.kotlinofficialgoodpractices.modules.exoplayer

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import es.rudo.kotlinofficialgoodpractices.R
import es.rudo.kotlinofficialgoodpractices.databinding.ActivityVideoBinding


class VideoActivity : AppCompatActivity() {

    private lateinit var binding: ActivityVideoBinding
    private lateinit var viewModel: VideoViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_video)
        viewModel = ViewModelProvider(this).get(VideoViewModel::class.java)

        binding.lifecycleOwner = this
        binding.videoActivity = this
        binding.videoViewModel = viewModel

        initObservers()
    }

    private fun initObservers() {

    }
}