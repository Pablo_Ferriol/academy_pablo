package es.rudo.kotlinofficialgoodpractices

import android.app.Application
import com.stripe.android.PaymentConfiguration
import es.rudo.kotlinofficialgoodpractices.helpers.Constants

class App : Application() {

    companion object {
        lateinit var instance: App private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}