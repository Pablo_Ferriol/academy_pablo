package es.rudo.kotlinofficialgoodpractices.modules.stripe.advanced

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.rudo.kotlinofficialgoodpractices.api.RetrofitClient
import es.rudo.kotlinofficialgoodpractices.data.model.Cart
import es.rudo.kotlinofficialgoodpractices.data.model.CartItem
import es.rudo.kotlinofficialgoodpractices.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class CartViewModel : ViewModel() {

    var itemList = MutableLiveData<ArrayList<CartItem>>()
    var cart: Cart = Cart()
    var isCartEmpty = MutableLiveData<Boolean>()

    init {
        CoroutineScope(Dispatchers.IO).launch {
            RetrofitClient().apiCall({
                RetrofitClient().getItems()
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            itemList.value = response.body() as ArrayList<CartItem>
                        } else {
//                            showError.value = App.instance.getString(R.string.General_NetworkError)
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
//                        showError.value = App.instance.getString(R.string.General_NetworkError)
                    }
                })
        }
    }

    fun countCart() {
        isCartEmpty.value = cart.items.isEmpty()
    }
}