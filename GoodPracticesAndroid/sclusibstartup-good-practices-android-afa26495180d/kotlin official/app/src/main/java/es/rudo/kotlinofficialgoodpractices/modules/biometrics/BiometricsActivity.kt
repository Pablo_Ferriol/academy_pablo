package es.rudo.kotlinofficialgoodpractices.modules.biometrics

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import es.rudo.kotlinofficialgoodpractices.R
import es.rudo.kotlinofficialgoodpractices.databinding.ActivityBiometricsBinding
import es.rudo.kotlinofficialgoodpractices.helpers.Constants
import es.rudo.kotlinofficialgoodpractices.helpers.Utils
import java.util.concurrent.Executor


class BiometricsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityBiometricsBinding
    private lateinit var viewModel: BiometricsViewModel

    private lateinit var executor: Executor
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_biometrics)
        viewModel = ViewModelProvider(this).get(BiometricsViewModel::class.java)

        binding.lifecycleOwner = this
        binding.biometricsActivity = this
        binding.biometricsViewModel = viewModel

        initObservers()

        checkBiometrics()
    }

    private fun checkBiometrics() {

        when (Utils.hasBiometrics(this)) {
            Constants.BIOMETRIC_AVAILABLE -> showBiometrics()
            Constants.BIOMETRIC_NOT_REGISTERED -> showRegisterBiometrics()
            Constants.BIOMETRIC_UNAVAILABLE -> showBiometrics()
            else -> showBiometrics()
        }
    }

    private fun showRegisterBiometrics() {
        startActivity(Intent(Settings.ACTION_SECURITY_SETTINGS))
    }

    private fun showBiometrics() {
        executor = ContextCompat.getMainExecutor(this)
        biometricPrompt = BiometricPrompt(this, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(
                    errorCode: Int,
                    errString: CharSequence
                ) {
                    super.onAuthenticationError(errorCode, errString)
                    Toast.makeText(
                        applicationContext,
                        "Authentication error: $errString", Toast.LENGTH_SHORT
                    )
                        .show()
                }

                override fun onAuthenticationSucceeded(
                    result: BiometricPrompt.AuthenticationResult
                ) {
                    super.onAuthenticationSucceeded(result)
                    Toast.makeText(
                        applicationContext,
                        "Authentication succeeded!", Toast.LENGTH_SHORT
                    )
                        .show()
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    Toast.makeText(
                        applicationContext, "Authentication failed",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            })
        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Biometric login for my app")
            .setSubtitle("Log in using your biometric credential")
            .setNegativeButtonText("Use account password")
            .build()

        biometricPrompt.authenticate(promptInfo)
    }

    private fun initObservers() {

    }
}