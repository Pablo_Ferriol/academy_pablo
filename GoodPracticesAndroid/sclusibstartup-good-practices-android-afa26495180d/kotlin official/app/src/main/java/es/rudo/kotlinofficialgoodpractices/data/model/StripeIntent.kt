package es.rudo.kotlinofficialgoodpractices.data.model

class StripeIntent {
    var payment_intent_id: String? = null
    var stripe_account: String? = null
    var customer_id: String? = null
    var stripe_version: String? = null
    var clientSecret: String? = null
    var susbscription_id: String? = null
    var source: String? = null //card id

}