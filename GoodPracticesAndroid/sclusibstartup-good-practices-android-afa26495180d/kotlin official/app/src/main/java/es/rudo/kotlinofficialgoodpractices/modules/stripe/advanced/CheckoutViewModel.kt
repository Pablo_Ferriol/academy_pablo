package es.rudo.kotlinofficialgoodpractices.modules.stripe.advanced

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.stripe.android.model.PaymentMethod
import es.rudo.kotlinofficialgoodpractices.api.RetrofitClient
import es.rudo.kotlinofficialgoodpractices.data.model.Cart
import es.rudo.kotlinofficialgoodpractices.data.model.StripeIntent
import es.rudo.kotlinofficialgoodpractices.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class CheckoutViewModel : ViewModel() {

    var cart = Cart()
    lateinit var paymentIntentClientSecret: String
    var paymentMethod: PaymentMethod? = null
    var isPaymentSuccess = MutableLiveData<Boolean>()
    var showError = MutableLiveData<String>()


    /**
     * GETS CLIENT SECRET BASED ON CART DATA
     */
    fun requestPaymentIntent(customerId: String?) {
//        cart.customer_id = customerId
//        cart.currency = "usd"
//        cart.total = 100.0
//        val items: ArrayList<CartItem> = ArrayList()
//        val cartItem = CartItem()
//        cartItem.id = "ADF"
//        cartItem.name = "Zapatillas deporte Nike"
//        cartItem.price = 100.0
//        items.add(cartItem)
//        cart.items = items

        cart.customer_id = customerId

        CoroutineScope(Dispatchers.IO).launch {
            RetrofitClient().apiCall({
                RetrofitClient().createPaymentIntent(cart)
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_CREATED_CODE) {
                            val clientSecret = response.body() as StripeIntent
                            paymentIntentClientSecret = clientSecret.clientSecret.toString()
                        } else {
                            showError.value = "Error creating intent session"
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                        showError.value = "Network error"
                    }
                })
        }

    }

    /**
     * CONFIRMS TO BACKEND THAT THE PAYMENT WAS SUCSESSFUL
     */
    fun confirmPayment(paymentId: String?) {
        val stripeIntent = StripeIntent()
        stripeIntent.payment_intent_id = paymentId
        CoroutineScope(Dispatchers.IO).launch {
            RetrofitClient().apiCall({
                RetrofitClient().confirmPayment(stripeIntent)
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_CREATED_CODE) {
                            isPaymentSuccess.value = true
                        } else {
                            showError.value = "Error during payment"
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                        showError.value = "Network error"
                    }
                })
        }
    }
}