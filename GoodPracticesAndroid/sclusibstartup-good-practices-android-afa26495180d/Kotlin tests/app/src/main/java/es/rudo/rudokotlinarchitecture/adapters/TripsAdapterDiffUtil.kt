package es.rudo.rudokotlinarchitecture.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import es.rudo.rudokotlinarchitecture.data.model.City
import es.rudo.rudokotlinarchitecture.databinding.ItemTripBinding


//class TripsAdapterDiffUtil(private val clickListener: TripListener) :
//    ListAdapter<City, TripsAdapterDiffUtil.ViewHolder>(TripsAdapterDiffCallback()) {
//
//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.bind(getItem(position)!!, clickListener)
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        return ViewHolder.from(parent)
//    }
//
//    class ViewHolder private constructor(val binding: ItemTripBinding) :
//        RecyclerView.ViewHolder(binding.root) {
//
//        fun bind(item: City, clickListener: TripListener) {
//            binding.city = item
//            binding.clickListener = clickListener
//            binding.executePendingBindings()
//
//            binding.buttonItem.setOnClickListener {
//                clickListener.onClick(item.id)
//            }
//
//            Glide.with(itemView).load(item.medias?.midsize).into(binding.imageBackground)
//        }
//
//        companion object {
//            fun from(parent: ViewGroup): ViewHolder {
//                val layoutInflater = LayoutInflater.from(parent.context)
//                val binding = ItemTripBinding.inflate(layoutInflater, parent, false)
//                return ViewHolder(binding)
//            }
//        }
//    }
//
//    class TripsAdapterDiffCallback : DiffUtil.ItemCallback<City>() {
//        override fun areItemsTheSame(oldItem: City, newItem: City): Boolean {
//            return oldItem.id == newItem.id
//        }
//
//        @SuppressLint("DiffUtilEquals")
//        override fun areContentsTheSame(oldItem: City, newItem: City): Boolean {
//            return oldItem == newItem
//        }
//    }
//
//    interface TripListener {
//        fun onClick(cityId : Int)
//        fun onDeleted(cityId: Int)
//    }
//
//}