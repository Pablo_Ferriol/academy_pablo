package es.rudo.rudokotlinarchitecture.helpers.extensions

import es.rudo.rudokotlinarchitecture.helpers.Constants

fun String.isValidEmail() = matches(Constants.EMAIL_PATTERN.toRegex())