package es.rudo.rudokotlinarchitecture.api

import es.rudo.rudokotlinarchitecture.data.model.City
import es.rudo.rudokotlinarchitecture.data.model.Login
import es.rudo.rudokotlinarchitecture.data.model.User
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST


interface Api {

    @GET("/rates")
    fun getRates(): Observable<List<String>>

    //LOGIN
    @POST("auth/token/")
    suspend fun postLogin(@Body login: Login): Response<Login>

    //GET ME
    @GET("users/me/")
    suspend fun getMe(): Response<User>

    //GET CITIES
    @GET("cities/")
    suspend fun getCities(): Response<Pager<City>>

}