<?xml version="1.0"?>
<recipe>

	<merge from="root/AndroidManifest.xml.ftl"
             to="${escapeXmlAttribute(manifestOut)}/AndroidManifest.xml" />

	<instantiate from="root/res/layout/mvvm_layout.xml"
                   to="${escapeXmlAttribute(resOut)}/layout/${layoutName}.xml" />

	<instantiate from="root/src/app_package/MVVMViewModel.kt.ftl"
                 to="${escapeXmlAttribute(srcOut)}/${className}ViewModel.kt" />

    <instantiate from="root/src/app_package/MVVMActivity.kt.ftl"
                 to="${escapeXmlAttribute(srcOut)}/${className}Activity.kt" />

    <open file="${srcOut}/${className}ViewModel.kt"/>
    <open file="${srcOut}/${className}Activity.kt"/>
    <open file="${escapeXmlAttribute(resOut)}/layout/${layoutName}.xml"/>

</recipe>