package es.rudo.bootcamp.adapters;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import es.rudo.bootcamp.R;
import es.rudo.bootcamp.entities.User;

public class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.ViewHolder> implements Filterable {

    ArrayList<User> users;
    ArrayList<User> usersDataFilter;
    private RecyclerViewClickInterface recyclerViewClickInterface;

    public RecycleAdapter(ArrayList<User> users, RecyclerViewClickInterface recyclerViewClickInterface) {
        this.users = users;
        this.recyclerViewClickInterface = recyclerViewClickInterface;
        this.usersDataFilter = new ArrayList<>(users);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();//change to viewBinding?
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_search_user, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        String name = users.get(i).getUsername();
        int url = users.get(i).getAvatar();
        viewHolder.txtView.setText(name);
        Picasso.get().load(url).into(viewHolder.imgView);
        if (users.get(i).getFollowed()) {
            changeButtonFollow(viewHolder.btnView);
        }
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtView;
        private Button btnView;
        private ImageView imgView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView); //change to viewBinding?
            txtView = itemView.findViewById(R.id.text_username);
            btnView = itemView.findViewById(R.id.button_follow);
            imgView = itemView.findViewById(R.id.image_user);

            btnView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (btnView.getText().equals("Seguir")) {
                        changeButtonFollow(btnView);
                        users.get(getAdapterPosition()).setFollowed(true);
                    } else {
                        changeButtonUnFollow(btnView);
                        users.get(getAdapterPosition()).setFollowed(false);
                    }
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recyclerViewClickInterface.onItemClick(getAdapterPosition());
                }
            });
        }
    }

    @Override
    public Filter getFilter() {
        return recycleFilter;
    }

    private Filter recycleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            ArrayList<User> filteredList = new ArrayList<>();
            if (charSequence == null || charSequence.length() == 0) { //if not have param to search, show all
                filteredList.addAll(usersDataFilter);
            } else {
                String filterPattern = charSequence.toString().trim();
                for (User user : usersDataFilter) { // filter
                    if (user.getUsername().contains(filterPattern) || user.getEmail().contains(filterPattern)) {
                        filteredList.add(user);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            users.clear();
            users.addAll((ArrayList) filterResults.values);
            notifyDataSetChanged();
        }
    };

    //Interface
    public interface RecyclerViewClickInterface {
        void onItemClick(int position);
    }

    private void changeButtonFollow(Button button) {
        button.setText("Siguiendo");
        button.setBackgroundColor(Color.parseColor("#B9B8B8"));
    }

    private void changeButtonUnFollow(Button button) {
        button.setText("Seguir");
        button.setBackgroundColor(Color.parseColor("#E91E1E"));
    }
}

