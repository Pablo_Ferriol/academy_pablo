package es.rudo.bootcamp.utils;

public class Constants {
    //Checks
    public static final String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public static final String PASSWORD_PATTERN = "([A-Z])+([a-z])+([0-9])+";
    public static final int PASSWORD_LENGTH = 7;
    //Keys shared
    public static final String PREF_TEXT = "PREF_TEXT";
    public static final String PREF_CHECK = "PREF_CHECK";
    public static final String PREF_SWITCH = "PREF_SWITCH";
    //Splas screen
    public static int SPLASH_TIME_OUT = 3000;
}
