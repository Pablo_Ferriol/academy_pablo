package es.rudo.bootcamp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

import es.rudo.bootcamp.databinding.ActivityRudoLogoBinding;
import es.rudo.bootcamp.utils.Constants;

public class RudoLogoActivity extends AppCompatActivity {

    private ActivityRudoLogoBinding binding;
    //private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRudoLogoBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        //check token and redirect
        //FirebaseUser currentUser = mAuth.getCurrentUser();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(RudoLogoActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }, Constants.SPLASH_TIME_OUT);
    }

}
