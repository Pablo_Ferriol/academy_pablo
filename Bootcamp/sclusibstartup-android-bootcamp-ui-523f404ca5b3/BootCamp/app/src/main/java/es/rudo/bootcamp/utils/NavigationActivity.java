package es.rudo.bootcamp.utils;


import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import es.rudo.bootcamp.ui.FunnyImageFragment;
import es.rudo.bootcamp.ui.MapsFragment;
import es.rudo.bootcamp.ui.SharedPreferencesFragment;
import es.rudo.bootcamp.R;
import es.rudo.bootcamp.databinding.ActivityNavigationBinding;
import es.rudo.bootcamp.ui.fullname.UserNameFragment;
import es.rudo.bootcamp.ui.HomeFragment;

public class NavigationActivity extends AppCompatActivity {
    private ActivityNavigationBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityNavigationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        BottomNavigationView bottomNav = binding.bnvNavBar;
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        Fragment defaultFragment = new HomeFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, defaultFragment).commit();
    }

    BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment selectedFragment = null;
            switch (menuItem.getItemId()) {
                case R.id.nav_home:
                    selectedFragment = new HomeFragment();
                    break;
                case R.id.nav_search:
                    selectedFragment = new UserNameFragment();
                    break;
                case R.id.nav_add:
                    selectedFragment = new SharedPreferencesFragment();
                    break;
                case R.id.nav_profile:
                    selectedFragment = new FunnyImageFragment();
                    break;
                case R.id.nav_maps:
                    selectedFragment = new MapsFragment();
                    break;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
            return true;
        }
    };
}
