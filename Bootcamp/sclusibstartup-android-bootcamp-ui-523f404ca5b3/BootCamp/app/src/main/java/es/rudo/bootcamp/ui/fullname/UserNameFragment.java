package es.rudo.bootcamp.ui.fullname;

import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import es.rudo.bootcamp.R;

public class UserNameFragment extends Fragment {

    private es.rudo.bootcamp.databinding.FragmentUserNameBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = es.rudo.bootcamp.databinding.FragmentUserNameBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendValue();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding = null;
    }

    public UserNameFragment() {
        // Required empty public constructor
    }

    public void sendValue() {
        String message = getString(R.string.alert_emptyFields);
        String name = binding.editName.getText().toString();
        if (!name.isEmpty()) {
            Intent intent = new Intent(getContext(), SurnameActivity.class);
            intent.putExtra("INTENT_NAME", binding.editName.getText().toString());
            startActivity(intent);
        } else {
            binding.editName.setError(message);
        }
    }
}