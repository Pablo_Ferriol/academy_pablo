package es.rudo.bootcamp.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import es.rudo.bootcamp.databinding.FragmentSharedPreferencesBinding;
import es.rudo.bootcamp.utils.Constants;

public class SharedPreferencesFragment extends Fragment {

    private FragmentSharedPreferencesBinding binding;

    private SharedPreferences sharedPreferences;
    private String sharedFiles = "es.rudo.androiduibootcamp.Data";
    private SharedPreferences.Editor editor;

    private final String Text = Constants.PREF_TEXT;
    private final String Check = Constants.PREF_CHECK;
    private final String Switch = Constants.PREF_SWITCH;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //add this.getActivity() to get context
        sharedPreferences = this.getActivity().getSharedPreferences(sharedFiles, Context.MODE_PRIVATE);
        chargeDataCache();
        binding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveDataCache();
            }
        });
        binding.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cleanDataCache();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSharedPreferencesBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    public SharedPreferencesFragment() {
        // Required empty public constructor
    }

    public void saveDataCache() {
        editor = sharedPreferences.edit();
        editor.putString(Text, binding.etEx6EditText.getText().toString());
        editor.putBoolean(Check, binding.cbEx6CheckBox.isChecked());
        editor.putBoolean(Switch, binding.swEx6Switch.isChecked());
        editor.apply();
    }

    private void chargeDataCache() {
        String text = sharedPreferences.getString(Text, null);
        boolean check = sharedPreferences.getBoolean(Check, false);
        boolean isSwitch = sharedPreferences.getBoolean(Switch, false);
        binding.etEx6EditText.setText(text);
        binding.cbEx6CheckBox.setChecked(check);
        binding.swEx6Switch.setChecked(isSwitch);
    }

    public void cleanDataCache() {
        binding.etEx6EditText.setText("");
        binding.cbEx6CheckBox.setChecked(false);
        binding.swEx6Switch.setChecked(false);
        editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}