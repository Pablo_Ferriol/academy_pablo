package es.rudo.bootcamp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import es.rudo.bootcamp.R;

import es.rudo.bootcamp.databinding.ActivityUserRegisterBinding;
import es.rudo.bootcamp.utils.Constants;
import es.rudo.bootcamp.utils.NavigationActivity;

public class UserRegisterActivity extends AppCompatActivity {

    private ActivityUserRegisterBinding binding;
    private FirebaseAuth mAuth;
    private String message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityUserRegisterBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        mAuth = FirebaseAuth.getInstance();

        binding.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                regUser();
            }
        });
    }

    private boolean checkName() {
        message = getString(R.string.alert_emptyFields);
        EditText textUsername = binding.editUsername;
        String name = textUsername.getText().toString();

        if (name.isEmpty()) {
            textUsername.setError(message);
            return false;
        } else if (name.length() < 2) {
            textUsername.setError("Minimo 2 carácteres");
            return false;
        } else {
            textUsername.setError(null);
        }
        return true;
    }

    private boolean checkEmail() {
        message = getString(R.string.alert_emptyFields);
        EditText textEmail = binding.editEmail;
        String email = textEmail.getText().toString();
        String regxEmailPattern = Constants.EMAIL_PATTERN;

        if (email.isEmpty()) {
            textEmail.setError(message);
            return false;
        } else if (!email.matches(regxEmailPattern)) {
            textEmail.setError("Email incorrecto");
            return false;
        } else {
            textEmail.setError(null);
        }
        return true;
    }

    private boolean checkPassword() {
        message = getString(R.string.alert_emptyFields);
        EditText textPassword = binding.editPassword;
        String password = textPassword.getText().toString();
        String passwordPattern = Constants.PASSWORD_PATTERN;

        if (password.isEmpty()) {
            textPassword.setError(message);
            return false;
        } else if (!password.matches(passwordPattern)) {
            textPassword.setError("Tiene que contener al menos un carácter, mayus número y minuscula");
            return false;
        } else if (password.length() < Constants.PASSWORD_LENGTH) {
            textPassword.setError("Al menos 8 carácteres");
            return false;
        } else {
            textPassword.setError(null);
        }
        return true;
    }

    private boolean checkConfirm() {
        message = getString(R.string.alert_emptyFields);
        EditText textPassword = binding.editPassword;
        EditText textConfPassword = binding.editConfirmPassword;
        String confirmPass = textConfPassword.getText().toString();
        String password = textPassword.getText().toString();

        if (confirmPass.isEmpty()) {
            textConfPassword.setError(message);
            return false;
        } else if (!confirmPass.equals(password)) {
            textConfPassword.setError("No coinciden las contraseñas");
            return false;
        } else {
            textConfPassword.setError(null);
        }
        return true;
    }

    private boolean checkTerms() {
        if (!binding.cbTerms.isChecked()) {
            binding.cbTerms.setError("");
            return false;
        } else {
            binding.cbTerms.setError(null);
        }
        return true;
    }

    public void createUser(String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            Intent intent = new Intent(UserRegisterActivity.this, NavigationActivity.class);
                            startActivity(intent);
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(UserRegisterActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void regUser() {
        String email = binding.editEmail.getText().toString();
        String password = binding.editPassword.getText().toString();

        boolean check;
        check = checkName();
        check = checkEmail();
        check = checkPassword();
        check = checkConfirm();

        if (check && checkTerms()) {
            createUser(email, password);
        }
    }
}
