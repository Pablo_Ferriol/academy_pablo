package es.rudo.bootcamp.ui.fullname;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import es.rudo.bootcamp.R;
import es.rudo.bootcamp.databinding.ActivitySurnameBinding;

public class SurnameActivity extends AppCompatActivity {

    private ActivitySurnameBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySurnameBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        getData("INTENT_NAME", binding.textName);
    }

    public void sendValue(View view) {
        String message = getString(R.string.alert_emptyFields);
        String name = binding.textName.getText().toString();
        String surname = binding.editSurname.getText().toString();

        if (!surname.isEmpty()) {
            Intent intent = new Intent(this, FullNameActivity.class);
            intent.putExtra("INTENT_FULLNAME", name + " " + binding.editSurname.getText().toString());
            startActivity(intent);
        } else {
            binding.editSurname.setError(message);
        }
    }

    private void getData(String key, TextView textView) {
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        String fullName = bundle.getString(key);
        textView.setText(fullName);
    }
}
