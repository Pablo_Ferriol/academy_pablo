package es.rudo.bootcamp.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import es.rudo.bootcamp.R;
import es.rudo.bootcamp.databinding.ActivityLoginBinding;
import es.rudo.bootcamp.utils.Constants;
import es.rudo.bootcamp.utils.NavigationActivity;

public class LoginActivity extends AppCompatActivity {

    private ActivityLoginBinding binding;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        //firebase login
        mAuth = FirebaseAuth.getInstance();
        setContentView(view);
        textForgottenPassword();
        textNewAccount();

        binding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

    }

    private void textForgottenPassword() {
        SpannableString string = new SpannableString("¿No recuerdas tu contraseña?");
        UnderlineSpan undrlnSpan = new UnderlineSpan();
        string.setSpan(undrlnSpan, 0, string.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        binding.textForgottenPass.setText(string);
    }

    private void textNewAccount() {
        SpannableString string = new SpannableString("Crea tu nueva cuenta");
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.MAGENTA);
        UnderlineSpan underlineSpan = new UnderlineSpan();
        string.setSpan(colorSpan, 0, string.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        string.setSpan(underlineSpan, 0, string.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        binding.textNewAccountSecond.setText(string);
    }

    public void showMessageForgottenMessage(View view) {
        Toast.makeText(this, "No recuerdas tu contraseña?", Toast.LENGTH_SHORT).show();
    }

    public void newAccount(View view) {
        Intent intent = new Intent(this, UserRegisterActivity.class);
        startActivity(intent);
    }

    private boolean checkEmail() {
        String message = getString(R.string.alert_emptyFields);
        EditText textEmail = binding.editLoginEmail;
        String email = textEmail.getText().toString();
        String regxEmailPattern = Constants.EMAIL_PATTERN;

        if (email.isEmpty()) {
            textEmail.setError(message);
            return false;
        } else {
            textEmail.setError(null);
        }
        return true;
    }

    private boolean checkPassword() {
        String message = getString(R.string.alert_emptyFields);
        EditText textPassword = binding.editLoginPassword;
        String password = textPassword.getText().toString();
        String passwordPattern = Constants.PASSWORD_PATTERN;

        if (password.isEmpty()) {
            textPassword.setError(message);
            return false;
        } else {
            textPassword.setError(null);
        }
        return true;
    }

    public void login() {
        if (checkEmail() && checkPassword()) {
            String username = binding.editLoginEmail.getText().toString();
            String password = binding.editLoginPassword.getText().toString();
            signIn(username, password);
        }
    }

    public void signIn(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            //updateUI(user);
                            Intent intent = new Intent(LoginActivity.this, NavigationActivity.class);
                            startActivity(intent);
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(LoginActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }
                    }
                });
    }
}
