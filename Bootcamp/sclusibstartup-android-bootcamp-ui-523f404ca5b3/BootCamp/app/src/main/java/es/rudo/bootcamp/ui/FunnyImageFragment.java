package es.rudo.bootcamp.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import es.rudo.bootcamp.databinding.FragmentFunnyImgBinding;

public class FunnyImageFragment extends Fragment {

    private FragmentFunnyImgBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentFunnyImgBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String imageUri = "https://images7.memedroid.com/images/UPLOADED915/5b606b55be7f9.jpeg";
        ImageView imgView = binding.imgFunnyImage;
        Picasso.get()
                .load(imageUri)
                .into(imgView);
    }

    public FunnyImageFragment() {
        // Required empty public constructor
    }
}