package es.rudo.bootcamp.ui;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;

import es.rudo.bootcamp.R;
import es.rudo.bootcamp.adapters.RecycleAdapter;
import es.rudo.bootcamp.databinding.FragmentHomeBinding;
import es.rudo.bootcamp.entities.User;

public class HomeFragment extends Fragment implements RecycleAdapter.RecyclerViewClickInterface {

    private FragmentHomeBinding binding;
    private ArrayList<User> users;

    public HomeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //region List Users
        User user0 = new User("Pablo", "Pablo@gmail.com", true, R.drawable.equipo1);
        User user1 = new User("Jesus", "Jesus@gmail.com", true, R.drawable.equipo1);
        User user2 = new User("Juan", "Juan@gmail.com", true, R.drawable.equipo2);
        User user3 = new User("Rafa", "Rafa@gmail.com", false, R.drawable.equipo1);
        User user4 = new User("Damia", "Damia@gmail.com", true, R.drawable.equipo3);
        User user5 = new User("Pablo", "Pablo@gmail.com", false, R.drawable.equipo2);
        User user6 = new User("Jesus", "Jesus@gmail.com", true, R.drawable.equipo1);
        User user7 = new User("Juan", "Juan@gmail.com", false, R.drawable.equipo3);
        User user8 = new User("Rafa", "Rafa@gmail.com", true, R.drawable.equipo1);
        User user9 = new User("Damia", "Damia@gmail.com", false, R.drawable.equipo2);
        User user10 = new User("Pablo", "Pablo@gmail.com", true, R.drawable.equipo1);
        User user11 = new User("Jesus", "Jesus@gmail.com", false, R.drawable.equipo3);
        User user12 = new User("Juan", "Juan@gmail.com", true, R.drawable.equipo2);
        User user13 = new User("Rafa", "Rafa@gmail.com", false, R.drawable.equipo1);
        User user14 = new User("Damia", "Damia@gmail.com", false, R.drawable.equipo1);
        users = new ArrayList<>();
        users.add(user0);
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        users.add(user5);
        users.add(user6);
        users.add(user7);
        users.add(user8);
        users.add(user9);
        users.add(user10);
        users.add(user11);
        users.add(user12);
        users.add(user13);
        users.add(user14);
        //endregion
        final RecycleAdapter adapter = new RecycleAdapter(users, this);
        binding.recyclerUsers.setAdapter(adapter);
        binding.recyclerUsers.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.searchUsers.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onItemClick(int position) {
        Toast.makeText(getContext(), users.get(position).getEmail(), Toast.LENGTH_SHORT).show();
    }

}