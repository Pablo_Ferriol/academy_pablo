package es.rudo.chatripp.api;

import android.util.Log;

import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;

import java.util.concurrent.TimeUnit;

import es.rudo.chatripp.App;
import es.rudo.chatripp.BuildConfig;
import es.rudo.chatripp.entities.Chat;
import es.rudo.chatripp.entities.Cities;
import es.rudo.chatripp.entities.Login;
import es.rudo.chatripp.entities.Profile;
import es.rudo.chatripp.entities.Token;
import es.rudo.chatripp.entities.Trip;
import es.rudo.chatripp.utils.Constants;
import okhttp3.OkHttpClient;
import okhttp3.internal.platform.Platform;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class DataWebService extends DataStrategy {

    private Retrofit retrofit;
    private Retrofit retrofitLogin;
    private ApiService apiService;
    private ApiService apiserviceLogin;
    private String TAG_FAILURE = "FAILURE_CALL";


    public DataWebService() {
        OkHttpClient clientWithoutAuth = new OkHttpClient.Builder()
                .addInterceptor(new LoggingInterceptor.Builder()
                        .loggable(BuildConfig.DEBUG)
                        .setLevel(Level.BODY)
                        .log(Platform.INFO)
                        .request("Request")
                        .response("Response")
                        .build())
                .build();

        OkHttpClient clientWithAuth = new OkHttpClient.Builder()
                .addInterceptor(new LoggingInterceptor.Builder()
                        .loggable(BuildConfig.DEBUG)
                        .setLevel(Level.BODY)
                        .log(Platform.INFO)
                        .request("Request")
                        .response("Response")
                        .build())
                .addInterceptor(new AccessTokenInterceptor())
                .authenticator(new AccessTokenAuthenticator())
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(Config.API_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientWithAuth)
                .build();

        apiService = retrofit.create(ApiService.class);

        retrofitLogin = new Retrofit.Builder()
                .baseUrl(Config.API_URL)
                .client(clientWithoutAuth)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiserviceLogin = retrofitLogin.create(ApiService.class);
    }

    @Override
    public Call<Token> refreshToken() {
        ApiService apiServiceRefresh = retrofitLogin.create(ApiService.class);
        Login login = new Login();
        login.setRefresh_token(App.preferences.getRefreshToken());
        login.setGrant_type(Config.GRANT_TYPE);

        return apiServiceRefresh.refreshToken(login);
    }

    @Override
    public void postLogin(Login login, InteractDispatcherObject interactDispatcherObject) {
        apiserviceLogin.postLogin(login).enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                interactDispatcherObject.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                interactDispatcherObject.response(Constants.SERVER_TIMEOUT_CODE, null);
                Log.e(TAG_FAILURE, "onFailure: post login - " + t.getMessage());
            }
        });
    }

    @Override
    public void postRegister(Profile profile, InteractDispatcherObject interactDispatcherObject) {
        apiserviceLogin.postRegister(profile).enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                interactDispatcherObject.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                interactDispatcherObject.response(Constants.SERVER_TIMEOUT_CODE, null);
                Log.e(TAG_FAILURE, "onFailure: post register - " + t.getMessage());
            }
        });
    }

    @Override
    public void getMe(InteractDispatcherObject interactDispatcherObject) {
        apiService.getMe().enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                interactDispatcherObject.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                Log.e(TAG_FAILURE, "onFailure: get me - " + t.getMessage());
            }
        });
    }

    @Override
    public void getChats(DataStrategy.InteractDispatcherObject interactDispatcherObject) {
        apiService.getChats().enqueue(new Callback<Chat>() {
            @Override
            public void onResponse(Call<Chat> call, Response<Chat> response) {
                interactDispatcherObject.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Chat> call, Throwable t) {
                Log.e(TAG_FAILURE, "onFailure: get chats - " + t.getMessage());
            }
        });
    }

    @Override
    public void getTrips(int page, InteractDispatcherPager interactDispatcherPager) {
        apiService.getTrips(page).enqueue(new Callback<Pager<Trip>>() {
            @Override
            public void onResponse(Call<Pager<Trip>> call, Response<Pager<Trip>> response) {
                interactDispatcherPager.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Pager<Trip>> call, Throwable t) {

            }
        });
    }

    @Override
    public void getCities(int page, InteractDispatcherPager interactDispatcherPager) {
        apiService.getAllTrips(page).enqueue(new Callback<Pager<Cities>>() {
            @Override
            public void onResponse(Call<Pager<Cities>> call, Response<Pager<Cities>> response) {
                interactDispatcherPager.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Pager<Cities>> call, Throwable t) {

            }
        });
    }

    @Override
    public void getFriends(String user, int page, InteractDispatcherPager interactDispatcherPager) {
        apiService.getFriends(user, page).enqueue(new Callback<Pager<Profile>>() {
            @Override
            public void onResponse(Call<Pager<Profile>> call, Response<Pager<Profile>> response) {
                interactDispatcherPager.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Pager<Profile>> call, Throwable t) {

            }
        });
    }


}
