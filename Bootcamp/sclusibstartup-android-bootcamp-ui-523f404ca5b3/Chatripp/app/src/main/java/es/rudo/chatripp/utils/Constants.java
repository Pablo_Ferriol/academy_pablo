package es.rudo.chatripp.utils;

public class Constants {
    //server response
    public static final int SERVER_TIMEOUT_CODE = 408;
    public static final int SERVER_SUCCESS_CODE = 200;
    public static final int SERVER_CREATED_CODE = 201;
    public static final int SERVER_BADREQUEST_CODE = 400;
    //Checks
    public static final String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public static final String PASSWORD_PATTERN = "([A-Z])+([a-z])+([0-9])+";
    public static final int PASSWORD_LENGTH = 7;
    //Date
    public static final String SHORT_DATE_PATTERN = "dd MMM";
    public static final String SERVER_DATE_PATTERN = "yyyy-MM-dd";
    //Media
    public static final int MEDIA_POS = 0;
    //Log
    public static final String TAG_FAILURE = "FAILURE_CALL";
    //Pagination
    public static final int PAGINATION = 1;
    public static final int MAX_PAGINATION = 15;

}
