package es.rudo.chatripp.adapters;


import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import es.rudo.chatripp.R;
import es.rudo.chatripp.entities.Profile;
import es.rudo.chatripp.utils.Constants;


public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.ViewHolder> implements Filterable {

    List<Profile> friends;
    List<Profile> friendsDataFilter;
    RecyclerViewClickInterface recyclerViewClickInterface;

    public FriendsAdapter(List<Profile> friends, RecyclerViewClickInterface recyclerViewClickInterface) {
        this.friends = friends;
        this.recyclerViewClickInterface = recyclerViewClickInterface;
        this.friendsDataFilter = new ArrayList<>(friends);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_search_user, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        String text_username;
        text_username = friends.get(i).getFirst_name();
        viewHolder.text_username.setText(text_username);
        setImageAdapter(viewHolder, i);
        changeButtonFollow(viewHolder.button_follow, i);
    }

    @Override
    public int getItemCount() {
        return friends.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView text_username;
        private Button button_follow;
        private ImageView image_user;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            text_username = itemView.findViewById(R.id.text_username);
            button_follow = itemView.findViewById(R.id.button_follow);
            image_user = itemView.findViewById(R.id.image_user);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recyclerViewClickInterface.onItemClick(friends.get(getAdapterPosition()));
                }
            });
        }
    }

    @Override
    public Filter getFilter() {
        return recycleFilter;
    }

    private Filter recycleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            ArrayList<Profile> filteredList = new ArrayList<>();
            if (charSequence == null || charSequence.length() == 0) {
                filteredList.addAll(friendsDataFilter);
            } else {
                String filterPattern = charSequence.toString().trim();
                for (Profile profile : friendsDataFilter) { // filter
                    if (profile.getFirst_name().contains(filterPattern) || profile.getUsername().contains(filterPattern)) {
                        filteredList.add(profile);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            friends.clear();
            friends.addAll((List<Profile>) filterResults.values);
            notifyDataSetChanged();
        }
    };

    public interface RecyclerViewClickInterface {
        void onItemClick(Profile profile);
    }

    private void changeButtonFollow(Button button, int position) {
        String isFriend = friends.get(position).getIs_friend();
        if (isFriend.equals("Accepted")) {
            button.setText(R.string.friend_state_friend);
        } else if (isFriend.equals("None")) {
            button.setText(R.string.friend_state_none);
        } else if (isFriend.equals("Pending")) {
            button.setText(R.string.friend_state_pending);
        }
    }

    private void setImageAdapter(ViewHolder holder, int position) {
        try {
            if (!(friends.get(position).getImage_medias() == null)) {
                String url = friends.get(position).getImage_medias().getFile();
                Picasso.get().load(url).into(holder.image_user);
            } else {
                holder.image_user.setImageResource(R.drawable.noimagefound);
            }
        } catch (Exception e) {
            Log.e(Constants.TAG_FAILURE, "onFailure: PicassoFail - " + e.getMessage());
        }
    }
}

