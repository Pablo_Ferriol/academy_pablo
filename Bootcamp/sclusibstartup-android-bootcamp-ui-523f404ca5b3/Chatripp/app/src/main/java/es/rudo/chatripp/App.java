package es.rudo.chatripp;

import android.app.Application;

import es.rudo.chatripp.utils.AppPreferences;

public class App extends Application {
    private static final String FATAL_NO_INSTANCE = "Fatal Error: No App instance found";

    private static App instance;
    public static AppPreferences preferences;
//    public static NavigationHelper navigationHelper;
//    public static TinyDB db;
//    public static Profile profile;

    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());//TODO activar en versiones para el cliente.
//        db = new TinyDB(getApplicationContext());
        preferences = new AppPreferences();
        instance = this;
    }

    public static synchronized App getInstance() {

        if (instance == null) {
            throw new IllegalArgumentException(FATAL_NO_INSTANCE);
        }

        return instance;
    }
}
