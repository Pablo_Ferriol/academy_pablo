package es.rudo.chatripp.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utils {

    public static String dateFormat(String date) {
        Locale local = new Locale("es", "ES");
        try {
            SimpleDateFormat formatServer = new SimpleDateFormat(Constants.SERVER_DATE_PATTERN, Locale.ENGLISH);
            Date dateParsed = formatServer.parse(date);
            SimpleDateFormat formatOutput = new SimpleDateFormat(Constants.SHORT_DATE_PATTERN, local);
            return formatOutput.format(dateParsed);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
