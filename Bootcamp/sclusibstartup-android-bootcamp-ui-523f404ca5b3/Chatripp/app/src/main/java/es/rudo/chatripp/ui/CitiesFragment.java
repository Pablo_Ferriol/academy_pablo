package es.rudo.chatripp.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import es.rudo.chatripp.adapters.CitiesAdapter;
import es.rudo.chatripp.api.DataStrategy;
import es.rudo.chatripp.api.DataWebService;
import es.rudo.chatripp.api.Pager;
import es.rudo.chatripp.databinding.FragmentCitiesBinding;
import es.rudo.chatripp.entities.Cities;
import es.rudo.chatripp.utils.Constants;

public class CitiesFragment extends Fragment {

    private FragmentCitiesBinding binding;
    private List<Cities> cities = new ArrayList<>();
    private CitiesAdapter adapter;
    private int page = Constants.PAGINATION;
    private String nextPage;

    public CitiesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCitiesBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.recycleCities.setLayoutManager(new LinearLayoutManager(getContext()));
        getCities(page);
        initListeners();
    }

    private void initListeners() {
        binding.nestedScrollCities.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            //check bottom poss and try charge next page.
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
                    if (nextPage != null) {
                        binding.progessBarCities.setVisibility(View.VISIBLE);
                        page++;
                        getCities(page);
                    }
                }
            }
        });
    }

    private void getCities(int page) {
        new DataWebService().getCities(page, new DataStrategy.InteractDispatcherPager<Cities>() {
            @Override
            public void response(int code, Pager<Cities> pager) {
                if (code == Constants.SERVER_SUCCESS_CODE) {
                    nextPage = pager.getNext();
                    cities.addAll(pager.getResults());
                    adapter = new CitiesAdapter(cities);
                    binding.recycleCities.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(getContext(), "Nothing to show", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
