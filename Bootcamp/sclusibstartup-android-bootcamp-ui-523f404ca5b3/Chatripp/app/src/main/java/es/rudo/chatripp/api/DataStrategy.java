package es.rudo.chatripp.api;

import es.rudo.chatripp.entities.Chat;
import es.rudo.chatripp.entities.Cities;
import es.rudo.chatripp.entities.Login;
import es.rudo.chatripp.entities.Profile;
import es.rudo.chatripp.entities.Token;
import es.rudo.chatripp.entities.Trip;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public abstract class DataStrategy {

    public static Object InteractDispatcherObject;

    public abstract Call<Token> refreshToken();

    public abstract void postRegister(Profile profile, InteractDispatcherObject interactDispatcherObject);

    public abstract void getMe(InteractDispatcherObject interactDispatcherObject);

    public abstract void getChats(InteractDispatcherObject interactDispatcherObject);

    public abstract void getTrips(int page, InteractDispatcherPager interactDispatcherPager);

    public abstract void getCities(int page, InteractDispatcherPager interactDispatcherPager);

    public abstract void getFriends(String user, int page, InteractDispatcherPager interactDispatcherPager);

    public interface InteractDispatcherObject<T> {
        void response(int code, T object);
    }

    public interface InteractDispatcherPager<T> {
        void response(int code, Pager<T> pager);
    }

    public abstract void postLogin(Login login, InteractDispatcherObject interactDispatcherObject);

    public interface ApiService {

        //REFRESH TOKEN
        @POST("auth/token/")
        Call<Token> refreshToken(@Body Login login);

        //REGISTER
        @POST("auth/register/")
        Call<Profile> postRegister(@Body Profile profile);

        //LOGIN
        @POST("auth/token/")
        Call<Token> postLogin(@Body Login login);

        //GET ME
        @GET("users/me/")
        Call<Profile> getMe();

        //GET CHATS
        @GET("chats/")
        Call<Chat> getChats();

        //GET TRIPS
        @GET("trips/")
        Call<Pager<Trip>> getTrips(
                @Query("page") int page
        );

        //GET CITIES
        @GET("cities/")
        Call<Pager<Cities>> getAllTrips(
                @Query("page") int page
        );

        //GET FRIENDS
        @GET("users/friends/")
        Call<Pager<Profile>> getFriends(
                @Query("user") String user,
                @Query("page") int page
        );
    }
}
