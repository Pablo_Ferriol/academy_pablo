package es.rudo.chatripp.ui.fragmentsProfile;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import es.rudo.chatripp.App;
import es.rudo.chatripp.adapters.FriendsAdapter;
import es.rudo.chatripp.api.DataStrategy;
import es.rudo.chatripp.api.DataWebService;
import es.rudo.chatripp.api.Pager;
import es.rudo.chatripp.databinding.FragmentFriendsBinding;
import es.rudo.chatripp.entities.Profile;
import es.rudo.chatripp.utils.Constants;

public class FriendsFragment extends Fragment implements FriendsAdapter.RecyclerViewClickInterface {

    private FragmentFriendsBinding binding;
    private List<Profile> friends = new ArrayList<>();
    private int page = Constants.PAGINATION;
    private FriendsAdapter adapter;
    private String nextPage;

    public FriendsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentFriendsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.recycleProfile.setLayoutManager(new LinearLayoutManager(getContext()));
        getDataFriends(page);
        initListeners();
    }

    private void initListeners() {
        binding.searchUsers.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.nestedScrollFriends.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            //check bottom poss and try charge next page.
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
                    if (nextPage != null) {
                        binding.progessBarFriends.setVisibility(View.VISIBLE);
                        page++;
                        getDataFriends(page);
                    }
                }
            }
        });
    }

    @Override
    public void onItemClick(Profile profile) {
        Toast.makeText(getContext(), profile.getUsername(), Toast.LENGTH_SHORT).show();
    }

    private void getDataFriends(int page) {
        String userId = App.preferences.getUserId();
        new DataWebService().getFriends(userId, page, new DataStrategy.InteractDispatcherPager<Profile>() {
            @Override
            public void response(int code, Pager<Profile> pager) {
                binding.progessBarFriends.setVisibility(View.GONE);
                nextPage = pager.getNext();
                friends.addAll(pager.getResults());
                adapter = new FriendsAdapter(friends, FriendsFragment.this);
                binding.recycleProfile.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        });
    }
}