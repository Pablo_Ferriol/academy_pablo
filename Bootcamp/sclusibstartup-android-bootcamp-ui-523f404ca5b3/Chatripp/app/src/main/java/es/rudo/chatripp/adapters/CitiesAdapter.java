package es.rudo.chatripp.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import es.rudo.chatripp.R;
import es.rudo.chatripp.entities.Cities;
import es.rudo.chatripp.utils.Constants;

public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.ViewHolder> {
    List<Cities> cities;

    public CitiesAdapter(List<Cities> cities) {
        this.cities = cities;
    }

    @NonNull
    @Override
    public CitiesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_cities, parent, false);
        return new CitiesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CitiesAdapter.ViewHolder holder, int position) {
        setDataTrip(holder, position);
    }

    @Override
    public int getItemCount() {
        return cities.size();
    }

    private void setDataTrip(@NonNull CitiesAdapter.ViewHolder holder, int position) {
        String textDestination, textCountry, imgUrl;
        textDestination = cities.get(position).getName();
        textCountry = cities.get(position).getCountry().getName();

        holder.textDestination.setText(textDestination);
        holder.textCountry.setText(textCountry);
        setImageAdapter(holder, position);

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textDestination;
        private TextView textCountry;
        private ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textDestination = itemView.findViewById(R.id.text_destination);
            textCountry = itemView.findViewById(R.id.text_country);
            imageView = itemView.findViewById(R.id.card_cities_background);
        }
    }

    private void setImageAdapter(ViewHolder holder, int position) {
        try {
            if (!(cities.get(position).getMedias().size() == 0)) {
                String imgUrl = cities.get(position).getMedias().get(Constants.MEDIA_POS).getFile();
                Picasso.get().load(imgUrl).into(holder.imageView);
            } else {
                holder.imageView.setImageResource(R.drawable.noimagefound);
            }
        } catch (Exception e) {
            Log.e(Constants.TAG_FAILURE, "onFailure: PicassoFail - " + e.getMessage());
        }
    }
}

