package es.rudo.chatripp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import es.rudo.chatripp.R;
import es.rudo.chatripp.api.DataStrategy;
import es.rudo.chatripp.api.DataWebService;
import es.rudo.chatripp.databinding.ActivityRegisterBinding;
import es.rudo.chatripp.entities.Profile;
import es.rudo.chatripp.utils.Constants;

public class RegisterActivity extends AppCompatActivity {

    private ActivityRegisterBinding binding;
    private String message;
    Button buttonRegister;
    EditText editUsername;
    EditText editEmail;
    EditText editPassword;
    EditText editConfirmPassword;
    CheckBox cbTerms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        //binding
        buttonRegister = binding.btnRegister;
        editUsername = binding.editUsername;
        editEmail = binding.editEmail;
        editPassword = binding.editPassword;
        editConfirmPassword = binding.editConfirmPassword;
        cbTerms = binding.cbTerms;

        initListeners();
    }

    private void initListeners() {
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                regUser();
            }
        });
    }

    private boolean checkName() {
        message = getString(R.string.alert_emptyFields);
        editUsername = binding.editUsername;
        String name = editUsername.getText().toString();

        if (name.isEmpty()) {
            editUsername.setError(message);
            return false;
        } else if (name.length() < 2) {
            editUsername.setError(getString(R.string.text_error_min_characters));
            return false;
        } else {
            editUsername.setError(null);
        }
        return true;
    }

    private boolean checkEmail() {
        message = getString(R.string.alert_emptyFields);
        String email = editEmail.getText().toString();
        String regxEmailPattern = Constants.EMAIL_PATTERN;

        if (email.isEmpty()) {
            editEmail.setError(message);
            return false;
        } else if (!email.matches(regxEmailPattern)) {
            editEmail.setError(getString(R.string.text_error_wrong_email));
            return false;
        } else {
            editEmail.setError(null);
        }
        return true;
    }

    private boolean checkPassword() {
        message = getString(R.string.alert_emptyFields);
        String password = editPassword.getText().toString();
        String passwordPattern = Constants.PASSWORD_PATTERN;

        if (password.isEmpty()) {
            editPassword.setError(message);
            return false;
        } else if (!password.matches(passwordPattern)) {
            editPassword.setError(getString(R.string.text_error_password_pattern));
            return false;
        } else if (password.length() < Constants.PASSWORD_LENGTH) {
            editPassword.setError(getString(R.string.text_error_min_password_characters));
            return false;
        } else {
            editPassword.setError(null);
        }
        return true;
    }

    private boolean checkConfirm() {
        message = getString(R.string.alert_emptyFields);
        String confirmPass = editConfirmPassword.getText().toString();
        String password = editPassword.getText().toString();

        if (confirmPass.isEmpty()) {
            editConfirmPassword.setError(message);
            return false;
        } else if (!confirmPass.equals(password)) {
            editConfirmPassword.setError(getString(R.string.text_error_match_passwords));
            return false;
        } else {
            editConfirmPassword.setError(null);
        }
        return true;
    }

    private boolean checkTerms() {
        if (!cbTerms.isChecked()) {
            cbTerms.setError("");
            return false;
        } else {
            cbTerms.setError(null);
        }
        return true;
    }

    public void createUser() {
        buttonRegister.setEnabled(false);
        Profile profile = new Profile();
        profile.setUsername(editUsername.getText().toString());
        profile.setEmail(editEmail.getText().toString());
        profile.setPassword(editPassword.getText().toString());
        profile.setPassword_confirm(editConfirmPassword.getText().toString());

        new DataWebService().postRegister(profile, new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                if (code == Constants.SERVER_CREATED_CODE) {
                    goToMainactivity();
                } else {
                    buttonRegister.setEnabled(true);
                    Toast.makeText(RegisterActivity.this, "Invalid Register", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void regUser() {
        boolean check;
        check = checkName();
        check = checkEmail();
        check = checkPassword();
        check = checkConfirm();

        if (check && checkTerms()) {
            createUser();
        }
    }

    private void goToMainactivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}