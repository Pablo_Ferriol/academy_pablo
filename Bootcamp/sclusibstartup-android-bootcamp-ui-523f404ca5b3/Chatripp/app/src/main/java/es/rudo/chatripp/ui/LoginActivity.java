package es.rudo.chatripp.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import es.rudo.chatripp.App;
import es.rudo.chatripp.R;
import es.rudo.chatripp.api.Config;
import es.rudo.chatripp.api.DataStrategy;
import es.rudo.chatripp.api.DataWebService;
import es.rudo.chatripp.databinding.ActivityLoginBinding;
import es.rudo.chatripp.entities.Login;
import es.rudo.chatripp.entities.Token;
import es.rudo.chatripp.utils.Constants;
import es.rudo.chatripp.utils.NavigateActivity;

public class LoginActivity extends AppCompatActivity {
    private ActivityLoginBinding binding;
    TextView textForgottenPass;
    TextView textNewAccountSecond;
    EditText editLoginEmail;
    EditText editLoginPassword;
    Button buttonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        //binding
        textForgottenPass = binding.textForgottenPass;
        textNewAccountSecond = binding.textNewAccountSecond;
        editLoginEmail = binding.editLoginEmail;
        editLoginPassword = binding.editLoginPassword;
        buttonLogin = binding.btnLogin;

        textForgottenPassword();
        textNewAccount();
        initListeners();
    }

    private void initListeners() {
        binding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isCheck()) signIn();
            }
        });
    }

    private void textForgottenPassword() {
        SpannableString string = new SpannableString("¿No recuerdas tu contraseña?");
        UnderlineSpan undrlnSpan = new UnderlineSpan();
        string.setSpan(undrlnSpan, 0, string.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        textForgottenPass.setText(string);
    }

    private void textNewAccount() {
        SpannableString string = new SpannableString("Crea tu nueva cuenta");
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.MAGENTA);
        UnderlineSpan underlineSpan = new UnderlineSpan();
        string.setSpan(colorSpan, 0, string.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        string.setSpan(underlineSpan, 0, string.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        textNewAccountSecond.setText(string);
    }

    public void showMessageForgottenMessage(View view) {
        Toast.makeText(this, "No recuerdas tu contraseña?", Toast.LENGTH_SHORT).show();
    }

    public void newAccount(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private boolean checkEmail() {
        String message = getString(R.string.alert_emptyFields);
        String email = editLoginEmail.getText().toString();

        if (email.isEmpty()) {
            editLoginEmail.setError(message);
            return false;
        } else {
            editLoginEmail.setError(null);
        }
        return true;
    }

    private boolean checkPassword() {
        String message = getString(R.string.alert_emptyFields);
        String password = editLoginPassword.getText().toString();

        if (password.isEmpty()) {
            editLoginPassword.setError(message);
            return false;
        } else {
            editLoginPassword.setError(null);
        }
        return true;
    }

    private Boolean isCheck() {
        if (checkEmail() && checkPassword()) {
            return true;
        } else {
            return false;
        }
    }

    public void signIn() {
        buttonLogin.setEnabled(false);
        Login login = new Login();
        login.setUsername(editLoginEmail.getText().toString());
        login.setPassword(editLoginPassword.getText().toString());
        login.setGrant_type(Config.GRANT_TYPE_LOGIN);

        new DataWebService().postLogin(login, new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                if (code == Constants.SERVER_SUCCESS_CODE) {
                    Token token = (Token) object;
                    String auth_token = token.getAccess_token();
                    App.preferences.setAccessToken(auth_token);
                    goToMainactivity();
                } else {
                    buttonLogin.setEnabled(true);
                    Toast.makeText(LoginActivity.this, "Invalid Login", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void goToMainactivity() {
        Intent intent = new Intent(this, NavigateActivity.class);
        startActivity(intent);
    }
}