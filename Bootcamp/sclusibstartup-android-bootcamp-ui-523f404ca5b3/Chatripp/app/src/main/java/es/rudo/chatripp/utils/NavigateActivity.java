package es.rudo.chatripp.utils;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import es.rudo.chatripp.R;
import es.rudo.chatripp.databinding.ActivityNavigateBinding;
import es.rudo.chatripp.ui.CitiesFragment;
import es.rudo.chatripp.ui.ProfileFragment;
import es.rudo.chatripp.ui.Test1Fragment;
import es.rudo.chatripp.ui.Test2Fragment;
import es.rudo.chatripp.ui.Test4Fragment;

public class NavigateActivity extends AppCompatActivity {
    private ActivityNavigateBinding binding;

    final Fragment profile = new ProfileFragment();
    final Fragment maps = new Test1Fragment();
    final Fragment chats = new Test2Fragment();
    final Fragment trips = new CitiesFragment();
    final Fragment favs = new Test4Fragment();
    final FragmentManager fragmentManager = getSupportFragmentManager();
    Fragment firstFragment;
    private int tabId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityNavigateBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        BottomNavigationView bottomNav = binding.navigateBar;
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        loadData();
        setCreateFragments(bottomNav, tabId);
    }

    //TODO; see implement stacks
    BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            tabId = item.getItemId();
            switch (tabId) {
                case R.id.nav_map:
                    fragmentManager.beginTransaction().hide(firstFragment).show(maps).commit();
                    firstFragment = maps;
                    break;
                case R.id.nav_chat:
                    fragmentManager.beginTransaction().hide(firstFragment).show(chats).commit();
                    firstFragment = chats;
                    break;
                case R.id.nav_trip:
                    fragmentManager.beginTransaction().hide(firstFragment).show(trips).commit();
                    firstFragment = trips;
                    break;
                case R.id.nav_profile:
                    fragmentManager.beginTransaction().hide(firstFragment).show(profile).commit();
                    firstFragment = profile;
                    break;
                case R.id.nav_favs:
                    fragmentManager.beginTransaction().hide(firstFragment).show(favs).commit();
                    firstFragment = favs;
                    break;
            }
            fragmentManager.beginTransaction().commit();
            return true;
        }
    };

    private void setCreateFragments(BottomNavigationView navigation, int tabId) {
        fragmentManager.beginTransaction().add(R.id.fragment_navigate, maps, "1").hide(maps).commit();
        fragmentManager.beginTransaction().add(R.id.fragment_navigate, chats, "2").hide(chats).commit();
        fragmentManager.beginTransaction().add(R.id.fragment_navigate, trips, "3").hide(trips).commit();
        fragmentManager.beginTransaction().add(R.id.fragment_navigate, profile, "4").hide(profile).commit();
        fragmentManager.beginTransaction().add(R.id.fragment_navigate, favs, "5").hide(favs).commit();

        if (tabId != -1) {
            switch (tabId) {
                case R.id.nav_map:
                    fragmentManager.beginTransaction().show(maps).commit();
                    firstFragment = maps;
                    break;
                case R.id.nav_chat:
                    fragmentManager.beginTransaction().show(chats).commit();
                    firstFragment = chats;
                    break;
                case R.id.nav_trip:
                    fragmentManager.beginTransaction().show(trips).commit();
                    firstFragment = trips;
                    break;
                case R.id.nav_profile:
                    fragmentManager.beginTransaction().show(profile).commit();
                    firstFragment = profile;
                    break;
                case R.id.nav_favs:
                    fragmentManager.beginTransaction().show(favs).commit();
                    firstFragment = favs;
                    break;
            }
            navigation.setSelectedItemId(tabId);
        } else {
            fragmentManager.beginTransaction().show(maps).commit();
            firstFragment = maps;
        }
        fragmentManager.beginTransaction().commit();
    }

    @SuppressLint("ApplySharedPref")
    private void saveData() {
        SharedPreferences preferences = getSharedPreferences("preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("tabId", tabId).commit();
    }

    private void loadData() {
        SharedPreferences preferences = getSharedPreferences("preferences", MODE_PRIVATE);
        tabId = preferences.getInt("tabId", -1);
    }

    @Override
    protected void onStop() {
        saveData();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.hide(firstFragment).commitAllowingStateLoss();
        super.onDestroy();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

}