package es.rudo.chatripp.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import es.rudo.chatripp.App;
import es.rudo.chatripp.R;
import es.rudo.chatripp.adapters.PageAdapter;
import es.rudo.chatripp.api.DataStrategy;
import es.rudo.chatripp.api.DataWebService;
import es.rudo.chatripp.databinding.FragmentProfileBinding;
import es.rudo.chatripp.entities.Chat;
import es.rudo.chatripp.entities.Profile;
import es.rudo.chatripp.ui.fragmentsProfile.FriendsFragment;
import es.rudo.chatripp.ui.fragmentsProfile.PostsFragment;
import es.rudo.chatripp.ui.fragmentsProfile.TripsFragment;
import es.rudo.chatripp.utils.Constants;

public class ProfileFragment extends Fragment {
    private FragmentProfileBinding binding;

    TabLayout tabLayout;
    ViewPager viewPager;
    FloatingActionButton floatButtonCollapseConf;
    AppBarLayout appBar;
    Toolbar toolbarProfile;
    CollapsingToolbarLayout collapseBar;
    ImageView imageProfileUser;

    public ProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentProfileBinding.inflate(inflater, container, false);
        //binding
        tabLayout = binding.tabsProfile;
        viewPager = binding.viewPageProfile;
        floatButtonCollapseConf = binding.floatButtonCollapseConf;
        appBar = binding.appBarProfile;
        toolbarProfile = binding.toolbarProfile;
        collapseBar = binding.collapseBar;
        imageProfileUser = binding.imageProfileUser;
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        initListeners();
        setCustomTabByProfile(tabLayout);
        setLoggedUserProfile();
    }

    private void initListeners() {
        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) - appBarLayout.getTotalScrollRange() == 0) {
                    //Collapsed
                    toolbarProfile.setVisibility(View.VISIBLE);
                    collapseBar.setCollapsedTitleTextColor(Color.parseColor("#000000"));
                    floatButtonCollapseConf.setVisibility(View.VISIBLE);
                } else {
                    //Expanded
                    toolbarProfile.setVisibility(View.INVISIBLE);
                    floatButtonCollapseConf.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private void setLoggedUserProfile() {
        new DataWebService().getMe(new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                Profile profile = (Profile) object;
                String urlProfileImage = profile.getImage_medias().getFile();

                collapseBar.setTitle(profile.getFirst_name());
                Picasso.get().load(urlProfileImage).into(imageProfileUser);
                App.preferences.setUserId(((Profile) object).getId()); //id on cache
            }
        });
    }

    private void setCustomTabByProfile(TabLayout tabLayout) {
        new DataWebService().getMe(new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                Profile profile = (Profile) object;
                String tripsCount = String.valueOf(profile.getUser_trips().size());
                String friendsCount = String.valueOf(profile.getFriends_count());
                try {
                    setCustomTab(tabLayout, getString(R.string.tab_custom_1), tripsCount, 1);
                    setCustomTab(tabLayout, getString(R.string.tab_custom_2), friendsCount, 2);
                } catch (Exception e) {
                    Log.e(Constants.TAG_FAILURE, "onFailure: get me - " + e.getMessage());
                }
            }
        });
        new DataWebService().getChats(new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                Chat chat = (Chat) object;
                String chatsCount = String.valueOf(chat.getCount());
                try {
                    setCustomTab(tabLayout, getString(R.string.tab_custom_0), chatsCount, 0);
                } catch (Exception e) {
                    Log.e(Constants.TAG_FAILURE, "onFailure: get me - " + e.getMessage());
                }
            }
        });
    }

    private void setCustomTab(TabLayout tabLayout, String tabName, String tabCount, int pos) {
        LinearLayout myTab = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.item_custom_tab, null);
        TextView textViewName = myTab.findViewById(R.id.text_tab);
        TextView textViewCount = myTab.findViewById(R.id.text_count);
        textViewName.setText(tabName);
        textViewCount.setText(tabCount);
        tabLayout.getTabAt(pos).setCustomView(myTab);
    }

    private void setViewPager(ViewPager viewPager) {
        PageAdapter pageAdapter = new PageAdapter(getFragmentManager());
        pageAdapter.addFragment(new PostsFragment(), getString(R.string.tab_custom_0));
        pageAdapter.addFragment(new TripsFragment(), getString(R.string.tab_custom_1));
        pageAdapter.addFragment(new FriendsFragment(), getString(R.string.tab_custom_2));
        viewPager.setAdapter(pageAdapter);
    }
}