package es.rudo.chatripp.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import es.rudo.chatripp.R;
import es.rudo.chatripp.entities.Trip;
import es.rudo.chatripp.utils.Constants;
import es.rudo.chatripp.utils.Utils;


public class TripsAdapter extends RecyclerView.Adapter<TripsAdapter.ViewHolder> {

    private List<Trip> trips;


    public TripsAdapter(List<Trip> trips) {
        this.trips = trips;
    }

    @NonNull
    @Override
    public TripsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_trips, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TripsAdapter.ViewHolder holder, int position) {
        setDataTrip(holder, position);
    }

    @Override
    public int getItemCount() {
        return trips.size();
    }

    private void setDataTrip(@NonNull ViewHolder holder, int position) {
        String dateInit, dataFinish, destinationText, countryText;

        dateInit = trips.get(position).getStart_date();
        dataFinish = trips.get(position).getEnd_date();
        destinationText = trips.get(position).getDestination().getName();
        countryText = trips.get(position).getDestination().getCountry().getName();

        holder.textDestination.setText(destinationText);
        holder.textDateInit.setText(Utils.dateFormat(dateInit));
        holder.textDateFinish.setText(Utils.dateFormat(dataFinish));
        holder.textCountry.setText(countryText);
        setImageAdapter(holder, position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textDestination;
        private TextView textDateInit;
        private TextView textDateFinish;
        private TextView textCountry;
        private ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textDestination = itemView.findViewById(R.id.text_destination);
            textDateInit = itemView.findViewById(R.id.text_data_init);
            textDateFinish = itemView.findViewById(R.id.text_data_finish);
            textCountry = itemView.findViewById(R.id.text_country);
            imageView = itemView.findViewById(R.id.card_trip_background);
        }
    }

    private void setImageAdapter(ViewHolder holder, int position) {
        try {
            if (!(trips.get(position).getDestination().getMedias().size() == 0)) {
                String url = trips.get(position).getDestination().getMedias().get(Constants.MEDIA_POS).getFile();
                Picasso.get().load(url).into(holder.imageView);
            } else {
                holder.imageView.setImageResource(R.drawable.noimagefound);
            }
        } catch (Exception e) {
            Log.e(Constants.TAG_FAILURE, "onFailure: PicassoFail - " + e.getMessage());
        }
    }
}

