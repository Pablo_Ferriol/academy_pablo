package es.rudo.chatripp.ui.fragmentsProfile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import es.rudo.chatripp.adapters.TripsAdapter;
import es.rudo.chatripp.api.DataStrategy;
import es.rudo.chatripp.api.DataWebService;
import es.rudo.chatripp.api.Pager;
import es.rudo.chatripp.databinding.FragmentTripsBinding;
import es.rudo.chatripp.entities.Trip;
import es.rudo.chatripp.utils.Constants;

public class TripsFragment extends Fragment {

    private FragmentTripsBinding binding;
    private TripsAdapter adapter;
    private List<Trip> trips = new ArrayList<>();
    private int page = Constants.PAGINATION;
    private int countObjectsPage = 0;
    private String nextPage;

    public TripsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentTripsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.recycleTrips.setLayoutManager(new LinearLayoutManager(getContext()));
        getDataTrips(page);
        initListeners();
    }

    private void initListeners() {

        binding.nestedScrollTrips.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            //check bottom poss and try charge next page.
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
                    if (nextPage != null) {
                        binding.progessBarTrips.setVisibility(View.VISIBLE);
                        page++;
                        getDataTrips(page);
                    }
                }
            }
        });
    }

    private void getDataTrips(int page) {
        new DataWebService().getTrips(page, new DataStrategy.InteractDispatcherPager<Trip>() {
            @Override
            public void response(int code, Pager<Trip> pager) {
                if (code == Constants.SERVER_SUCCESS_CODE) {
                    binding.progessBarTrips.setVisibility(View.GONE);
                    nextPage = pager.getNext();
                    trips.addAll(pager.getResults());
                    adapter = new TripsAdapter(trips);
                    binding.recycleTrips.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(getContext(), "Nothing to show", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}