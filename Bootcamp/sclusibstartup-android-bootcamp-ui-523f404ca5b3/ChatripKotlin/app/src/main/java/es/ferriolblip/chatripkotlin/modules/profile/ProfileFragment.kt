package es.ferriolblip.chatripkotlin.modules.profile

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.appbar.AppBarLayout.OnOffsetChangedListener
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.squareup.picasso.Picasso
import es.ferriolblip.chatripkotlin.R
import es.ferriolblip.chatripkotlin.adapters.ProfilePageAdapter
import es.ferriolblip.chatripkotlin.databinding.FragmentProfileBinding
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlin.math.abs

class ProfileFragment : Fragment() {

    private lateinit var binding: FragmentProfileBinding
    private lateinit var viewModel: ProfileViewModel
    private lateinit var adapter: ProfilePageAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        binding.lifecycleOwner = this
        binding.profileViewModel = viewModel
        adapter = ProfilePageAdapter(this)
        setProfilePageAdapter()
        setCollapsedStyle()
        initObservers()
        return binding.root
    }

    private fun setProfilePageAdapter() {
        binding.viewPageProfile.adapter = adapter
        TabLayoutMediator(binding.tabsProfile, binding.viewPageProfile) { _, _ ->
        }.attach()
    }

    private fun initObservers() {
        viewModel.imgUser.observe(viewLifecycleOwner, Observer {
            Picasso.get().load(it).into(binding.imageProfileUser)
        })

        viewModel.tripsCount.observe(viewLifecycleOwner, Observer {
            it?.let {
                setCustomTab(
                    binding.tabsProfile, getString(R.string.tab_profile_trips), it, 1
                )
            }
        })
        viewModel.friendsCount.observe(viewLifecycleOwner, Observer {
            it?.let {
                setCustomTab(binding.tabsProfile, getString(R.string.tab_profile_friends), it, 2)
            }
        })
        viewModel.chatsCount.observe(viewLifecycleOwner, Observer {
            it?.let {
                setCustomTab(binding.tabsProfile, getString(R.string.tab_profile_posts), it, 0)
            }
        })
    }

    private fun setCollapsedStyle() {
        binding.appBarProfile.addOnOffsetChangedListener(OnOffsetChangedListener { appBarLayout, verticalOffset ->
            when {
                //collapsed
                abs(verticalOffset) - appBarLayout.totalScrollRange == 0 -> {
                    binding.toolbarProfile.visibility = View.VISIBLE
                    binding.collapseBar.setCollapsedTitleTextColor(Color.parseColor("#000000"))
                    binding.floatButtonCollapseConf.visibility = View.VISIBLE
                }
                //extended
                else -> {
                    binding.toolbarProfile.visibility = View.INVISIBLE
                    binding.floatButtonCollapseConf.visibility = View.INVISIBLE
                }
            }
        })
    }

    private fun setCustomTab(tabLayout: TabLayout, tabName: String, tabCount: String, pos: Int) {
        val myTab =
            LayoutInflater.from(context).inflate(R.layout.item_custom_tab, null) as LinearLayout
        val textViewName = myTab.findViewById<TextView>(R.id.text_tab)
        val textViewCount = myTab.findViewById<TextView>(R.id.text_count)
        textViewCount.text = tabCount
        textViewName.text = tabName
        tabLayout.getTabAt(pos)?.customView = myTab
    }
}

