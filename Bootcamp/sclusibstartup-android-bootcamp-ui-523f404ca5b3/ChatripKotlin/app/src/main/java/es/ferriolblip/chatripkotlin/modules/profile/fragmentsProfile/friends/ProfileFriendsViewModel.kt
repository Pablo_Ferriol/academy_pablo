package es.ferriolblip.chatripkotlin.modules.profile.fragmentsProfile.friends

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.ferriolblip.chatripkotlin.App
import es.ferriolblip.chatripkotlin.api.Pager
import es.ferriolblip.chatripkotlin.api.RetrofitClient
import es.ferriolblip.chatripkotlin.data.model.Profile
import es.ferriolblip.chatripkotlin.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class ProfileFriendsViewModel : ViewModel() {
    private val retrofitClient: RetrofitClient = RetrofitClient()
    val friends = MutableLiveData<ArrayList<Profile>>()
    val list: ArrayList<Profile> = ArrayList()
    var page: Int = Constants.PAGINATION

    init {
        getFriends()
    }

    fun getFriends() {
        val user: String? = App.preferences.getUserId()
        CoroutineScope(Dispatchers.IO).launch {
            retrofitClient.apiCall({
                retrofitClient.getFriends(page, user)
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) { //TODO,improve
                            val responsePager: Pager<Profile> = response.body() as Pager<Profile>
                            responsePager.results?.let { list.addAll(it) }
                            friends.value = list
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }

                })
        }

    }

}
