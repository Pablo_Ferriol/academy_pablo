package es.ferriolblip.chatripkotlin.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import es.ferriolblip.chatripkotlin.R
import es.ferriolblip.chatripkotlin.data.model.City
import es.ferriolblip.chatripkotlin.databinding.ItemCitiesBinding

class CitiesAdapter(private val cities: ArrayList<City>) :
    RecyclerView.Adapter<CitiesAdapter.CitiesHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CitiesHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return CitiesHolder(layoutInflater.inflate((R.layout.item_cities), parent, false))
    }

    override fun onBindViewHolder(holder: CitiesHolder, position: Int) {
        holder.bind(cities[position])
    }

    override fun getItemCount(): Int {
        return cities.size
    }

    class CitiesHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemCitiesBinding.bind(view)
        fun bind(item: City?) {
            binding.textDestination.text = item?.name
            if (item?.medias?.size == 0) Picasso.get().load(R.drawable.noimagefound)
                .into(binding.cardCitiesBackground)
            else Picasso.get().load(item?.medias?.get(0)?.file).into(binding.cardCitiesBackground)
        }
    }
}