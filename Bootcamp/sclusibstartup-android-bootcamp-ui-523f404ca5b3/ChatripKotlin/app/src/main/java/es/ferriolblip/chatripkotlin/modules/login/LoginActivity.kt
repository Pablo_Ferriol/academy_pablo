package es.ferriolblip.chatripkotlin.modules.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import es.ferriolblip.chatripkotlin.R
import es.ferriolblip.chatripkotlin.databinding.ActivityLoginBinding
import es.ferriolblip.chatripkotlin.modules.navigate.NavigateActivity
import es.ferriolblip.chatripkotlin.modules.register.RegisterActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        binding.lifecycleOwner = this
        binding.loginViewModel = viewModel
        binding.loginActiviy = this

        initObservers()
    }

    private fun initObservers() {
        viewModel.eventIsOk.observe(this, Observer { isOk ->
            if (isOk) {
                goToNavigateActivity()
            }
        })

        viewModel.usernameError.observe(this, Observer { error ->
            input_username.error = error
        })

        viewModel.passwordError.observe(this, Observer { error ->
            input_password.error = error
        })
    }

    private fun goToNavigateActivity() {
        val intent = Intent(this, NavigateActivity::class.java)
        startActivity(intent)
    }

    fun goToRegister() {
        startActivity(Intent(this,RegisterActivity::class.java))
    }

    fun forgotPassToast(){
        Toast.makeText(this,"Meh, has olvidado tus datos?!", Toast.LENGTH_SHORT).show()
    }
}