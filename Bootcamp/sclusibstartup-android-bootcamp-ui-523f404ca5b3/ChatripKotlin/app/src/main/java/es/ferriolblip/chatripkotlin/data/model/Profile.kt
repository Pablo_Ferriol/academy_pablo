package es.ferriolblip.chatripkotlin.data.model

import java.io.Serializable

class Profile : Serializable {
    var id: String? = null
    var username: String? = null
    var password: String? = null
    var password_confirm: String? = null
    var first_name: String? = null
    var last_name: String? = null
    var country: String? = null
    var email: String? = null
    var device_id: String? = null
    var platform: String? = null
    var image_medias: Media? = null
    var cover_medias: Media? = null
    var birth_date: String? = null
    var gender: String? = null
    var bio: String? = null
    var is_friend: String? = null
    var friends_count = 0
    var user_trips: ArrayList<Any>? = null
}
