package es.ferriolblip.chatripkotlin.modules.profile

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.ferriolblip.chatripkotlin.api.RetrofitClient
import es.ferriolblip.chatripkotlin.data.model.Chat
import es.ferriolblip.chatripkotlin.data.model.Profile
import es.ferriolblip.chatripkotlin.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class ProfileViewModel : ViewModel() {

    private val retrofitClient: RetrofitClient =
        RetrofitClient()
    var imgUser = MutableLiveData<String>()
    var inputUsername = MutableLiveData<String>()
    var tripsCount = MutableLiveData<String>()
    var friendsCount = MutableLiveData<String>()
    var chatsCount = MutableLiveData<String>()

    init {
        getMe()
        getChats()
    }

    fun getMe() {
        CoroutineScope(Dispatchers.IO).launch {
            retrofitClient.apiCall({
                retrofitClient.getMe()
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            val responseProfile: Profile = response.body() as Profile
                            inputUsername.value = responseProfile.first_name
                            imgUser.value = responseProfile.image_medias?.file
                            tripsCount.value = responseProfile.user_trips?.size.toString()
                            friendsCount.value = responseProfile.friends_count.toString()
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }
                })
        }
    }

    fun getChats() {
        CoroutineScope(Dispatchers.IO).launch {
            retrofitClient.apiCall({
                retrofitClient.getChats()
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        val responseChat: Chat = response.body() as Chat
                        chatsCount.value = responseChat.count.toString()
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }
                })
        }
    }
}
