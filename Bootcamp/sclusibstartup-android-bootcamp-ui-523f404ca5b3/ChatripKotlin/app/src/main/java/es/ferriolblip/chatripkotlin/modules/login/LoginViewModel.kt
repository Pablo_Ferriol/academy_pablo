package es.ferriolblip.chatripkotlin.modules.login

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.ferriolblip.chatripkotlin.App
import es.ferriolblip.chatripkotlin.api.Config
import es.ferriolblip.chatripkotlin.api.RetrofitClient
import es.ferriolblip.chatripkotlin.data.model.Login
import es.ferriolblip.chatripkotlin.data.model.Profile
import es.ferriolblip.chatripkotlin.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class LoginViewModel : ViewModel() {

    private val retrofitClient: RetrofitClient =
        RetrofitClient()
    var username = MutableLiveData<String>()
    var password = MutableLiveData<String>()
    val usernameError = MutableLiveData<String>()
    val passwordError = MutableLiveData<String>()
    var eventIsOk = MutableLiveData<Boolean>()

    private fun postLogin() {
        val login = Login()
        login.client_id = Config.CLIENT_ID
        login.client_secret = Config.CLIENT_SECRET
        login.grant_type = Config.GRANT_TYPE_LOGIN
        login.username = username.value
        login.password = password.value

        CoroutineScope(Dispatchers.IO).launch {
            retrofitClient.apiCall({
                retrofitClient.postLogin(login)
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            val responseLogin: Login = response.body() as Login
                            App.preferences.setAccessToken(responseLogin.access_token)
                            App.preferences.setRefreshToken(responseLogin.refresh_token)
                            getIdUser()
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }
                })
        }
    }

    private fun getIdUser() {
        CoroutineScope(Dispatchers.IO).launch {
            retrofitClient.apiCall({
                retrofitClient.getMe()
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            val responseProfile: Profile = response.body() as Profile
                            App.preferences.setUserId(responseProfile.id)
                            eventIsOk.value = true
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }

                })
        }
    }

    fun check() {
        var error = false
        val textUsername: String? = username.value
        val textPassword: String? = password.value

        if (textUsername.isNullOrEmpty()) {
            usernameError.value = "Campos vacíos"
            error = true
        } else {
            usernameError.value = ""
        }

        if (textPassword.isNullOrEmpty()) {
            passwordError.value = "Campos vacíos"
            error = true
        } else {
            passwordError.value = ""
        }

        if (!error) postLogin()
    }
}
