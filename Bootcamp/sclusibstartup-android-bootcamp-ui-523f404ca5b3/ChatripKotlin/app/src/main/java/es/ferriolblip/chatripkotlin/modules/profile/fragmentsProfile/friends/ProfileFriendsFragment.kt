package es.ferriolblip.chatripkotlin.modules.profile.fragmentsProfile.friends

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import es.ferriolblip.chatripkotlin.R
import es.ferriolblip.chatripkotlin.adapters.FriendsAdapter
import es.ferriolblip.chatripkotlin.databinding.FragmentProfileFriendsBinding

class ProfileFriendsFragment : Fragment() {

    private lateinit var binding: FragmentProfileFriendsBinding
    private lateinit var viewModel: ProfileFriendsViewModel
    private lateinit var adapter: FriendsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_profile_friends, container, false)
        viewModel = ViewModelProvider(this).get(ProfileFriendsViewModel::class.java)
        binding.lifecycleOwner = this
        binding.recycleProfileFriends.layoutManager = LinearLayoutManager(context)
        viewModel.friends.observe(viewLifecycleOwner, Observer {
            adapter = FriendsAdapter(it)
            binding.recycleProfileFriends.adapter = adapter
        })

        return binding.root
    }
}