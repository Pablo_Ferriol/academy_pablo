package es.ferriolblip.chatripkotlin.data.model

class Trip {
    var id: String? = null
    var start_date: String? = null
    var end_date: String? = null
    var destination: Destination? = null
}