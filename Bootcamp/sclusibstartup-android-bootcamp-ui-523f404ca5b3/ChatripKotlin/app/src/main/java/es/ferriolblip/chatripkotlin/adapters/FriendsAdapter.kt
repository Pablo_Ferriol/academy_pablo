package es.ferriolblip.chatripkotlin.adapters

import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import es.ferriolblip.chatripkotlin.R
import es.ferriolblip.chatripkotlin.data.model.Profile
import es.ferriolblip.chatripkotlin.databinding.ItemSearchUserBinding

class FriendsAdapter(private val friends: ArrayList<Profile>) :
    RecyclerView.Adapter<FriendsAdapter.FriendsHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FriendsAdapter.FriendsHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return FriendsHolder(layoutInflater.inflate((R.layout.item_search_user), parent, false))
    }

    override fun getItemCount(): Int {
        return friends.size
    }

    override fun onBindViewHolder(holder: FriendsAdapter.FriendsHolder, position: Int) {
        holder.bind(friends[position])
    }

    class FriendsHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemSearchUserBinding.bind(view)
        fun bind(item: Profile?) {
            Picasso.get().load(item?.image_medias?.file).into(binding.imageUser)
            binding.textUsername.text = item?.first_name
        }

    }

}