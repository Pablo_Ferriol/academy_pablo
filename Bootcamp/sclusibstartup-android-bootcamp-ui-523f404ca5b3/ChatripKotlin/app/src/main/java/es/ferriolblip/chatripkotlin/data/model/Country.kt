package es.ferriolblip.chatripkotlin.data.model

class Country {
    var name: String? = null
    var code: String? = null
}