package es.ferriolblip.chatripkotlin.helpers

object Constants {
    //Checks
    val EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    val PASSWORD_LENGTH = 8
    val USERNAME_LENGTH = 2
    val PASSWORD_PATTERN = "([A-Z])+([a-z])+([0-9])+"

    //Status
    val SERVER_SUCCESS_CODE = 200
    val SERVER_CREATED_CODE = 201
    val SERVER_NOCONTENT_CODE = 204
    val SERVER_BADREQUEST_CODE = 400
    val SERVER_UNAUTHORIZED_CODE = 401
    val SERVER_FORBIDDEN_CODE = 403
    val SERVER_NOTFOUND_CODE = 404
    val SERVER_TIMEOUT_CODE = 408
    val SERVER_INTERNALSERVER_CODE = 500

    //TabLayout
    val PROFILE_TAB_COUNT = 3

    //Date
    val SHORT_DATE_PATTERN = "dd MMM"
    val SERVER_DATE_PATTERN = "yyyy-MM-dd"

    //Pagination
    val PAGINATION = 1
}