package es.ferriolblip.chatripkotlin.modules.profile.fragmentsProfile.trips

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import es.ferriolblip.chatripkotlin.R
import es.ferriolblip.chatripkotlin.adapters.TripsAdapter
import es.ferriolblip.chatripkotlin.data.model.Trip
import es.ferriolblip.chatripkotlin.databinding.FragmentProfileTripsBinding
import es.ferriolblip.chatripkotlin.helpers.Constants

class ProfileTripsFragment : Fragment() {

    private lateinit var binding: FragmentProfileTripsBinding
    private lateinit var viewModel: ProfileTripsViewModel
    private lateinit var adapter: TripsAdapter
    var page: Int = Constants.PAGINATION

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_profile_trips, container, false)
        viewModel = ViewModelProvider(this).get(ProfileTripsViewModel::class.java)
        binding.lifecycleOwner = this
        binding.recycleTrips.layoutManager = LinearLayoutManager(context)
        binding.progessBarTrips.visibility = View.GONE
        initRecycle()
        initListeners()
        return binding.root
    }

    fun initRecycle() {
        viewModel.trips.observe(viewLifecycleOwner, Observer {
            adapter = TripsAdapter(it)
            binding.recycleTrips.adapter = adapter
        })
    }

    fun initListeners() {
        binding.nesteScrollProfileTrips.setOnScrollChangeListener { v: NestedScrollView, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
                viewModel.nextPage.value?.let {
                    binding.progessBarTrips.visibility = View.VISIBLE
                    page++
                    viewModel.getTrips(page)
                }
            }
        }
    }
}