package es.ferriolblip.chatripkotlin.modules.register

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import es.ferriolblip.chatripkotlin.R
import es.ferriolblip.chatripkotlin.databinding.ActivityRegisterBinding
import es.ferriolblip.chatripkotlin.modules.login.LoginActivity
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    lateinit var binding: ActivityRegisterBinding
    lateinit var viewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        viewModel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        binding.lifecycleOwner = this
        binding.registerViewModel = viewModel
        initObservers()
    }

    private fun initObservers() {
        viewModel.usernameError.observe(this, Observer { error -> edit_username.error = error })
        viewModel.emailError.observe(this, Observer { error -> edit_email.error = error })
        viewModel.passwordError.observe(this, Observer { error -> edit_password.error = error })
        viewModel.confirmPasswordError.observe(
            this,
            Observer { error -> edit_confirmPassword.error = error })
        viewModel.eventIsOk.observe(this, Observer {
            if (it) {
                Toast.makeText(this, "Registro ok!", Toast.LENGTH_SHORT).show()
                gotToLogin()
            }
        })
        viewModel.showTastTerm.observe(this, Observer {
            if (it) Toast.makeText(this, "Acepta los terminos!", Toast.LENGTH_SHORT).show()
        })

        viewModel.showTastBadRequest.observe(this, Observer {
            if (it) Toast.makeText(this, "El usuario ya existe", Toast.LENGTH_SHORT).show()
        })
    }

    private fun gotToLogin() {
        startActivity(Intent(this, LoginActivity::class.java))
    }
}