package es.ferriolblip.chatripkotlin.data.model

class City {
    var id: String? = null
    var name: String? = null
    var country: Country? = null
    var medias: List<Media>? = null
}