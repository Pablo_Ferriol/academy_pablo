package es.ferriolblip.chatripkotlin.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import es.ferriolblip.chatripkotlin.R
import es.ferriolblip.chatripkotlin.data.model.Trip
import es.ferriolblip.chatripkotlin.databinding.ItemTripsBinding
import es.ferriolblip.chatripkotlin.helpers.Utils

class TripsAdapter(
    private val trips: ArrayList<Trip>
) : RecyclerView.Adapter<TripsAdapter.TripsHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TripsHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return TripsHolder(layoutInflater.inflate(R.layout.item_trips, parent, false))
    }

    override fun onBindViewHolder(holder: TripsHolder, position: Int) {
        holder.bind(trips[position])
    }

    override fun getItemCount(): Int {
        return trips.size
    }

    class TripsHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemTripsBinding.bind(view)
        fun bind(item: Trip?) {
            val trip: Trip = item as Trip
            setData(trip)
        }

        private fun setData(trip: Trip) {
            binding.textDestination.text = trip.destination?.name
            binding.textDataInit.text = Utils.dateFormat(trip.start_date)
            binding.textDataFinish.text = Utils.dateFormat(trip.end_date)
            binding.textCountry.text = trip.destination?.country?.name
            Picasso.get().load(trip.destination?.medias?.get(0)?.file)
                .into(binding.cardTripBackground)
        }
    }
}