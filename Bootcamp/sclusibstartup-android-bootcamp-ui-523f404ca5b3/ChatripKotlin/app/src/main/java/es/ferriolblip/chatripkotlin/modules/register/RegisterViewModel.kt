package es.ferriolblip.chatripkotlin.modules.register

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.ferriolblip.chatripkotlin.api.RetrofitClient
import es.ferriolblip.chatripkotlin.data.model.Profile
import es.ferriolblip.chatripkotlin.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class RegisterViewModel : ViewModel() {

    private val retrofitClient: RetrofitClient = RetrofitClient()
    val username = MutableLiveData<String>()
    val email = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    val confirPassword = MutableLiveData<String>()
    val usernameError = MutableLiveData<String>()
    val emailError = MutableLiveData<String>()
    val passwordError = MutableLiveData<String>()
    val confirmPasswordError = MutableLiveData<String>()
    val terms = MutableLiveData<Boolean>()
    val showTastTerm = MutableLiveData<Boolean>()
    val showTastBadRequest = MutableLiveData<Boolean>()

    /*    val messageBadRequest = MutableLiveData<String>()*/
    var eventRegisterError = MutableLiveData<Boolean>()
    var eventIsOk = MutableLiveData<Boolean>()

    private fun checkName() {
        if (username.value.isNullOrEmpty()) {
            usernameError.value = "Campos vacíos"
            eventRegisterError.value = true
        } else eventRegisterError.value = false
        username.value?.let {
            if (it.length < Constants.USERNAME_LENGTH) {
                usernameError.value = "Al menos dos caracteres"
                eventRegisterError.value = true
            } else eventRegisterError.value = false
        }
    }

    private fun checkEmail() {
        if (email.value.isNullOrEmpty()) {
            emailError.value = "Campos vacíos"
            eventRegisterError.value = true
        } else eventRegisterError.value = false
        email.value?.let {
            if (!it.matches(Constants.EMAIL_PATTERN.toRegex())) {
                emailError.value = "Formato incorrecto"
                eventRegisterError.value = true
            } else eventRegisterError.value = false
        }
    }

    private fun checkPassword() {
        if (password.value.isNullOrEmpty()) {
            passwordError.value = "Campos vacios"
            eventRegisterError.value = true
        } else eventRegisterError.value = false
        password.value?.let {
            if (!it.matches(Constants.PASSWORD_PATTERN.toRegex())) {
                passwordError.value =
                    "Tiene que contener mayus número y minuscula"
                eventRegisterError.value = true
            } else eventRegisterError.value = false
            if (it.length < Constants.PASSWORD_LENGTH) {
                passwordError.value = "Al menos 8 carácteres"
                eventRegisterError.value = true
            } else eventRegisterError.value = false
        }
    }

    private fun checkConfirm() {
        if (confirPassword.value.isNullOrEmpty()) {
            confirmPasswordError.value = "Campos vacíos"
            eventRegisterError.value = true
        } else eventRegisterError.value = false
        confirPassword.value?.let {
            if (it != password.value) {
                confirmPasswordError.value = "No coincide"
                eventRegisterError.value = true
            } else eventRegisterError.value = false
        }
    }

    private fun checkTerms() {
        if (terms.value != true) {
            showTastTerm.value = true
            eventRegisterError.value = true
        } else eventRegisterError.value = false
    }

    fun check() {
        checkName()
        checkEmail()
        checkPassword()
        checkConfirm()
        checkTerms()
        if (eventRegisterError.value != true) {
            postUser()
        }
    }

    fun postUser() {
        val profile = Profile()
        profile.username = username.value
        profile.email = email.value
        profile.password = password.value
        profile.password_confirm = confirPassword.value

        CoroutineScope(Dispatchers.IO).launch {
            retrofitClient.apiCall({
                retrofitClient.postRegister(profile)
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_CREATED_CODE) {
                            eventIsOk.value = true
                        } else {
                            showTastBadRequest.value = true
/*                            var profile: Profile = response.body() as Profile
                            when (true) {
                                !profile.username.isNullOrEmpty() -> {
                                    showTastBadRequest.value = true*/
                            /* messageBadRequest.value = "Ya existe un usuario con ese nombre"
                         }
                     }*/
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }

                })
        }
    }
}