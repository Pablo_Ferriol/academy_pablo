package es.ferriolblip.chatripkotlin.api

import android.graphics.pdf.PdfDocument
import es.ferriolblip.chatripkotlin.data.model.*
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query


interface Api {

    @GET("/rates")
    fun getRates(): Observable<List<String>>

    //LOGIN
    @POST("auth/token/")
    suspend fun postLogin(@Body login: Login): Response<Login>

    @POST("auth/register/")
    suspend fun postRegister(@Body profile: Profile): Response<Profile>

    //GET ME
    @GET("users/me/")
    suspend fun getMe(): Response<Profile>

    //GET Chats
    @GET("chats/")
    suspend fun getChats(): Response<Chat>

    //GET Trips
    @GET("trips/")
    suspend fun getTrips(@Query("page") page: Int): Response<Pager<Trip>>

    //GET CITIES
    @GET("cities/")
    suspend fun getCities(@Query("page") page: Int): Response<Pager<City>>

    //GET FRIENDS
    @GET("users/friends/")
    suspend fun getFriends(
        @Query("page") page: Int,
        @Query("user") user: String?
    ): Response<Pager<Profile>>

}