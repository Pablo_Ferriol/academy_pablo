package es.ferriolblip.chatripkotlin.api

class Pager<T> {
    var count = 0
    var next: String? = null
    var previous: String? = null
    var results: ArrayList<T>? = null
}