package es.ferriolblip.chatripkotlin.modules.cities

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.ferriolblip.chatripkotlin.api.Pager
import es.ferriolblip.chatripkotlin.api.RetrofitClient
import es.ferriolblip.chatripkotlin.data.model.City
import es.ferriolblip.chatripkotlin.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class CitieViewModel : ViewModel() {

    private val retrofitClient: RetrofitClient =
        RetrofitClient()

    /*val cities : MutableLiveData<ArrayList<City>> = MutableLiveData()*/
    /*  var cities : MutableList<City> = mutableListOf()*/
    val cities =
        MutableLiveData<ArrayList<City>>()     //Ocurre antes el adapter que la llamda si uso este mutable?, si utilizo el mutable y el observer si que ocurre antes la llamada.
    val nextPage =
        MutableLiveData<String>()                // Con mutablelive e iniciarlizandolo ocurre lo mismo, antes salta el adapter que la llamada, si lo inicio en el init.// El problema viene al inicializar
    var page: Int =
        Constants.PAGINATION                    // El problema viene al inicializar elñ mutable, hace que ocurra antes el adapter ¿?
    val list: ArrayList<City> = ArrayList()

    init {
        /*cities.value = ArrayList()*/
        getCities(page)
    }

    fun initMutable() {
        cities.value = ArrayList()
    }

    fun getCities(page: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            retrofitClient.apiCall({
                retrofitClient.getCities(page)
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        val responsePager: Pager<City> = response.body() as Pager<City>
                        /*cities.value = ArrayList()*/ // Aqui si que lo pilla, pero al inicializarlo cada vez es un nuevo array y se machaca.
                        nextPage.value = responsePager.next
                        /*   responsePager.results?.let { cities.value?.addAll(it) }*/
                        responsePager.results?.let { list.addAll(it) }
                        cities.value = list
                        /*     responsePager.results?.let { cities.addAll(it) }*/
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }
                })
        }
    }
}