package es.ferriolblip.chatripkotlin.helpers

import java.text.SimpleDateFormat
import java.util.*

object Utils {
    fun dateFormat(date: String?): String? {
        val local = Locale("es", "ES")
        val formatServer = SimpleDateFormat(Constants.SERVER_DATE_PATTERN, Locale.ENGLISH)
        val dateParsed = formatServer.parse(date)
        val formatOutput = SimpleDateFormat(Constants.SHORT_DATE_PATTERN, local)
        return formatOutput.format(dateParsed)
    }
}