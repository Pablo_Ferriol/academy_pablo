package es.ferriolblip.chatripkotlin.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import es.ferriolblip.chatripkotlin.modules.profile.ProfileFragment
import es.ferriolblip.chatripkotlin.modules.profile.fragmentsProfile.ProfileChatsFragment
import es.ferriolblip.chatripkotlin.modules.profile.fragmentsProfile.friends.ProfileFriendsFragment
import es.ferriolblip.chatripkotlin.modules.profile.fragmentsProfile.trips.ProfileTripsFragment

class ProfilePageAdapter(
    fragment: ProfileFragment,
) : FragmentStateAdapter(fragment) {

    var fragments: ArrayList<Fragment> = arrayListOf(
        ProfileChatsFragment(),
        ProfileTripsFragment(),
        ProfileFriendsFragment()
    )

    override fun getItemCount(): Int {
        return fragments.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragments[position]
    }
}