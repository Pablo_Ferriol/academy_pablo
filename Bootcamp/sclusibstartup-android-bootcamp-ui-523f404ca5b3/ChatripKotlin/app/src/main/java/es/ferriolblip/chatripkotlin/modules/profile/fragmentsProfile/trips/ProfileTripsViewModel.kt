package es.ferriolblip.chatripkotlin.modules.profile.fragmentsProfile.trips

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.ferriolblip.chatripkotlin.App
import es.ferriolblip.chatripkotlin.api.Pager
import es.ferriolblip.chatripkotlin.api.RetrofitClient
import es.ferriolblip.chatripkotlin.data.model.Trip
import es.ferriolblip.chatripkotlin.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class ProfileTripsViewModel : ViewModel() {
    private val retrofitClient: RetrofitClient =
        RetrofitClient()
    val trips = MutableLiveData<ArrayList<Trip>>()
    val list : ArrayList<Trip> = ArrayList()
    val nextPage = MutableLiveData<String>()
    var page: Int = Constants.PAGINATION

    init {
        getTrips(page)
    }

    fun getTrips(page: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            retrofitClient.apiCall({
                retrofitClient.getTrips(page)
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        val responsePager: Pager<Trip> = response.body() as Pager<Trip>
                        nextPage.value = responsePager.next
                        responsePager.results?.let { list.addAll(it) }
                        trips.value = list
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }
                })
        }
    }

}