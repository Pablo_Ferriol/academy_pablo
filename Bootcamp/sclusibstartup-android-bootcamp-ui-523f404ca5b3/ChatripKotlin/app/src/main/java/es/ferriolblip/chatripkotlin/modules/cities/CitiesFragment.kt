package es.ferriolblip.chatripkotlin.modules.cities

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import es.ferriolblip.chatripkotlin.R
import es.ferriolblip.chatripkotlin.adapters.CitiesAdapter
import es.ferriolblip.chatripkotlin.databinding.FragmentCitiesBinding
import es.ferriolblip.chatripkotlin.helpers.Constants

class CitiesFragment : Fragment() {

    private lateinit var binding: FragmentCitiesBinding
    private lateinit var viewModel: CitieViewModel
    private lateinit var adapter: CitiesAdapter
    var page: Int = Constants.PAGINATION

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_cities, container, false)
        viewModel = ViewModelProvider(this).get(CitieViewModel::class.java)
        binding.lifecycleOwner = this
        binding.progessBarCities.visibility = View.GONE
        binding.recycleCities.layoutManager = LinearLayoutManager(context)
        viewModel.initMutable()
        /* viewModel.cities.value = ArrayList()*/
        initRecycle()
        initListeners()
        return binding.root
    }

    private fun initRecycle() {
        /*viewModel.cities.value = ArrayList()*/
        viewModel.cities.observe(viewLifecycleOwner, Observer {
            adapter = CitiesAdapter(it)
            binding.recycleCities.adapter = adapter
        })
        /*       adapter = CitiesAdapter(viewModel.cities as ArrayList<City>)
               binding.recycleCities.adapter = adapter*/
    }

    private fun initListeners() {
        binding.nestedScrollCities.setOnScrollChangeListener { v: NestedScrollView, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
                viewModel.nextPage.value?.let {
                    binding.progessBarCities.visibility = View.VISIBLE
                    page++
                    viewModel.getCities(page)
                    adapter.notifyDataSetChanged()
                }
            }
        }
    }
}