package es.ferriolblip.chatripkotlin.data.model

class Destination {
    var id: String? = null
    var name: String? = null
    var country: Country? = null
    var medias: ArrayList<Media>? = null
}