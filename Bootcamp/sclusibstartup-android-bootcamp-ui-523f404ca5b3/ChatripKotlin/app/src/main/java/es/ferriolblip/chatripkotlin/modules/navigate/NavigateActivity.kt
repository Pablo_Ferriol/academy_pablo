package es.ferriolblip.chatripkotlin.modules.navigate

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomnavigation.BottomNavigationView
import es.ferriolblip.chatripkotlin.R
import es.ferriolblip.chatripkotlin.databinding.ActivityNavigateBinding
import es.ferriolblip.chatripkotlin.modules.chats.ChatsFragment
import es.ferriolblip.chatripkotlin.modules.cities.CitiesFragment
import es.ferriolblip.chatripkotlin.modules.favs.FavsFragment
import es.ferriolblip.chatripkotlin.modules.maps.MapsFragment
import es.ferriolblip.chatripkotlin.modules.profile.ProfileFragment

//TODO, adecuar la navegación a MVVM o bien estudiar mejor la de los ejemplos y aplicarla.
class NavigateActivity : AppCompatActivity() {

    private lateinit var binding: ActivityNavigateBinding
    private lateinit var viewModel: NavigateViewModel
    val fragmentManager = supportFragmentManager
    var firstFragment: Fragment? = null
    val profile: Fragment = ProfileFragment()
    val maps: Fragment = MapsFragment()
    val chats: Fragment = ChatsFragment()
    val trips: Fragment = CitiesFragment()
    val favs: Fragment = FavsFragment()
    private var tabId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_navigate)
        viewModel = ViewModelProvider(this).get(NavigateViewModel::class.java)
        binding.lifecycleOwner = this

        binding.navigateBar.setOnNavigationItemSelectedListener(navListener)
        loadData()
        setCreateFragments(binding.navigateBar, tabId)
    }

    var navListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            tabId = item.itemId
            when (tabId) {
                R.id.nav_map -> {
                    firstFragment?.let {
                        fragmentManager.beginTransaction().hide(it).show(maps).commit()
                    }
                    firstFragment = maps
                }
                R.id.nav_chat -> {
                    firstFragment?.let {
                        fragmentManager.beginTransaction().hide(it).show(chats).commit()
                    }
                    firstFragment = chats
                }
                R.id.nav_trip -> {
                    firstFragment?.let {
                        fragmentManager.beginTransaction().hide(it).show(trips).commit()
                    }
                    firstFragment = trips
                }
                R.id.nav_profile -> {
                    firstFragment?.let {
                        fragmentManager.beginTransaction().hide(it).show(profile).commit()
                    }
                    firstFragment = profile
                }
                R.id.nav_favs -> {
                    firstFragment?.let {
                        fragmentManager.beginTransaction().hide(it).show(favs).commit()
                    }
                    firstFragment = favs
                }
            }
            fragmentManager.beginTransaction().commit()
            true
        }

    private fun setCreateFragments(navigation: BottomNavigationView, tabId: Int) {
        fragmentManager.beginTransaction().add(R.id.fragment_navigate, maps, "1").hide(maps)
            .commit()
        fragmentManager.beginTransaction().add(R.id.fragment_navigate, chats, "2").hide(chats)
            .commit()
        fragmentManager.beginTransaction().add(R.id.fragment_navigate, trips, "3").hide(trips)
            .commit()
        fragmentManager.beginTransaction().add(R.id.fragment_navigate, profile, "4").hide(profile)
            .commit()
        fragmentManager.beginTransaction().add(R.id.fragment_navigate, favs, "5").hide(favs)
            .commit()
        if (tabId != -1) {
            when (tabId) {
                R.id.nav_map -> {
                    fragmentManager.beginTransaction().show(maps).commit()
                    firstFragment = maps
                }
                R.id.nav_chat -> {
                    fragmentManager.beginTransaction().show(chats).commit()
                    firstFragment = chats
                }
                R.id.nav_trip -> {
                    fragmentManager.beginTransaction().show(trips).commit()
                    firstFragment = trips
                }
                R.id.nav_profile -> {
                    fragmentManager.beginTransaction().show(profile).commit()
                    firstFragment = profile
                }
                R.id.nav_favs -> {
                    fragmentManager.beginTransaction().show(favs).commit()
                    firstFragment = favs
                }
            }
            navigation.selectedItemId = tabId
        } else {
            fragmentManager.beginTransaction().show(profile).commit()
            firstFragment = profile
        }
        fragmentManager.beginTransaction().commit()
    }

    @SuppressLint("ApplySharedPref")
    private fun saveData() {
        val preferences = getSharedPreferences("preferences", MODE_PRIVATE)
        val editor = preferences.edit()
        editor.putInt("tabId", tabId).commit()
    }

    private fun loadData() {
        val preferences = getSharedPreferences("preferences", MODE_PRIVATE)
        tabId = preferences.getInt("tabId", -1)
    }

    override fun onStop() {
        saveData()
        super.onStop()
    }

    override fun onDestroy() {
        val fragmentTransaction = fragmentManager.beginTransaction()
        firstFragment?.let { fragmentTransaction.hide(it).commitAllowingStateLoss() }
        super.onDestroy()
    }

    /*   protected fun onRestoreInstanceState(savedInstanceState: Bundle?) {
           super.onRestoreInstanceState(savedInstanceState!!)
       }*/
}