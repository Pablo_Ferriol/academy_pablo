package es.rudo.androiduibootcamp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.LinkedList;

import es.rudo.androiduibootcamp.Exercise06.Exercise06Activity;
import es.rudo.androiduibootcamp.Exercise07.Exercise07Activity;
import es.rudo.androiduibootcamp.databinding.ActivityMainBinding;
import es.rudo.androiduibootcamp.entities.User;
import es.rudo.androiduibootcamp.exercise01.Exercise01Activity;
import es.rudo.androiduibootcamp.exercise02.Excercise02Activity;
import es.rudo.androiduibootcamp.exercise03.Exercise03Activity;
import es.rudo.androiduibootcamp.exercise04.Exercise04_1Activity;
import es.rudo.androiduibootcamp.exercise05.Exercise05Activity;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
    }

    public void navButton1(View view) {
        Intent intent = new Intent(this, Exercise01Activity.class);
        startActivity(intent);
    }

    public void navButton2(View view) {
        Intent intent = new Intent(this, Excercise02Activity.class);
        startActivity(intent);
    }

    public void navButton3(View view) {
        Intent intent = new Intent(this, Exercise03Activity.class);
        startActivity(intent);
    }

    public void navButton4(View view) {
        Intent intent = new Intent(this, Exercise04_1Activity.class);
        startActivity(intent);
    }

    public void navButton5(View view) {
        Intent intent = new Intent(this, Exercise05Activity.class);
        startActivity(intent);
    }

    public void navButton6(View view) {
        Intent intent = new Intent(this, Exercise06Activity.class);
        startActivity(intent);
    }

    public void navButton7(View view) {
        Intent intent = new Intent(this, Exercise07Activity.class);
        startActivity(intent);
    }
}

