package es.rudo.androiduibootcamp.exercise01;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import es.rudo.androiduibootcamp.R;

public class Exercise01Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_excercise01);
    }
}
