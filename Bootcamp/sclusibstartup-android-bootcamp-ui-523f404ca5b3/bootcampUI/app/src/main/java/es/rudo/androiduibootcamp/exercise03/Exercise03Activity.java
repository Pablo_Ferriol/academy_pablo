package es.rudo.androiduibootcamp.exercise03;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import es.rudo.androiduibootcamp.R;
import es.rudo.androiduibootcamp.databinding.ActivityExercise03Binding;

public class Exercise03Activity extends AppCompatActivity {

    private ActivityExercise03Binding binding;
    private String message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityExercise03Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
    }

    private boolean checkName() {
        message = getString(R.string.alert_emptyFields);
        EditText textUsername = binding.editUsername;
        String name = textUsername.getText().toString();

        if (name.isEmpty()) {
            textUsername.setError(message);
            return false;
        } else if (name.length() < 2) {
            textUsername.setError("Minimo 2 carácteres");
            return false;
        } else {
            textUsername.setError(null);
        }
        return true;
    }

    private boolean checkEmail() {
        message = getString(R.string.alert_emptyFields);
        EditText textEmail = binding.editEmail;
        String email = textEmail.getText().toString();
        String regxEmailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (email.isEmpty()) {
            textEmail.setError(message);
            return false;
        } else if (!email.matches(regxEmailPattern)) {
            textEmail.setError("Email incorrecto");
            return false;
        } else {
            textEmail.setError(null);
        }
        return true;
    }

    private boolean checkPassword() {
        message = getString(R.string.alert_emptyFields);
        EditText textPassword = binding.editPassword;
        String password = textPassword.getText().toString();
        String passwordPattern = "([A-Z])+([a-z])+([0-9])+";

        if (password.isEmpty()) {
            textPassword.setError(message);
            return false;
        } else if (!password.matches(passwordPattern)) {
            textPassword.setError("Tiene que contener al menos un carácter, mayus número y minuscula");
            return false;
        } else if (password.length() < 7) {
            textPassword.setError("Al menos 8 carácteres");
            return false;
        } else {
            textPassword.setError(null);
        }
        return true;
    }

    private boolean checkConfirm() {
        message = getString(R.string.alert_emptyFields);
        EditText textPassword = binding.editPassword;
        EditText textConfPassword = binding.editConfirmPassword;
        String confirmPass = textConfPassword.getText().toString();
        String password = textPassword.getText().toString();

        if (confirmPass.isEmpty()) {
            textConfPassword.setError(message);
            return false;
        } else if (!confirmPass.equals(password)) {
            textConfPassword.setError("No coinciden las contraseñas");
            return false;
        } else {
            textConfPassword.setError(null);
        }
        return true;
    }

    private boolean checkTerms() {
        if (!binding.cbTerms.isChecked()) {
            binding.cbTerms.setError("");
            return false;
        } else {
            binding.cbTerms.setError(null);
        }
        return true;
    }

    public void regUser(View view) {
        boolean check;
        check = checkName();
        check = checkEmail();
        check = checkPassword();
        check = checkConfirm();

        if (check && checkTerms()) {
            Toast.makeText(this, "Usuario registrado correctamente", Toast.LENGTH_SHORT).show();
        }
    }
}
