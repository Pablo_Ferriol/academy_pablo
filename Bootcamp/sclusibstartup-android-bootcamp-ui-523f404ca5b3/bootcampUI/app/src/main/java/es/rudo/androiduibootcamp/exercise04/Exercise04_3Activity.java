package es.rudo.androiduibootcamp.exercise04;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import es.rudo.androiduibootcamp.databinding.ActivityExercise043Binding;

public class Exercise04_3Activity extends AppCompatActivity {
    private ActivityExercise043Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityExercise043Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        getData("INTENT_FULLNAME", binding.textFullname);
    }

    @Override
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void closeAp(View view) {
        finishAffinity();
    }

    private void getData(String key, TextView textView) {
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        String fullNAme = bundle.getString(key);
        textView.setText(fullNAme);
    }
}
