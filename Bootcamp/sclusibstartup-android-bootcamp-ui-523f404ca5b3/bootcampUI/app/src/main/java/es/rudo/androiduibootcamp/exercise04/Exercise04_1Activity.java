package es.rudo.androiduibootcamp.exercise04;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import es.rudo.androiduibootcamp.R;
import es.rudo.androiduibootcamp.databinding.ActivityExercise041Binding;

public class Exercise04_1Activity extends AppCompatActivity {

    private ActivityExercise041Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityExercise041Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
    }

    public void sendValue(View view) {
        String message = getString(R.string.alert_emptyFields);
        String name = binding.editName.getText().toString();
        if (!name.isEmpty()) {
            Intent intent = new Intent(this, Exercise04_2Activity.class);
            intent.putExtra("INTENT_NAME", binding.editName.getText().toString());
            startActivity(intent);
        } else {
            binding.editName.setError(message);
        }
    }
}
