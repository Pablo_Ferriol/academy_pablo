package es.rudo.androiduibootcamp.exercise04;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import es.rudo.androiduibootcamp.R;
import es.rudo.androiduibootcamp.databinding.ActivityExercise042Binding;

public class Exercise04_2Activity extends AppCompatActivity {

    private ActivityExercise042Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityExercise042Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        getData("INTENT_NAME", binding.textName);
    }

    public void sendValue(View view) {
        String message = getString(R.string.alert_emptyFields);
        String name = binding.textName.getText().toString();
        String surname = binding.editSurname.getText().toString();

        if (!surname.isEmpty()) {
            Intent intent = new Intent(this, Exercise04_3Activity.class);
            intent.putExtra("INTENT_FULLNAME", name + " " + binding.editSurname.getText().toString());
            startActivity(intent);
        } else {
            binding.editSurname.setError(message);
        }
    }

    private void getData(String key, TextView textView) {
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        String fullName = bundle.getString(key);
        textView.setText(fullName);
    }
}
