package es.rudo.androiduibootcamp.exercise02;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Toast;

import es.rudo.androiduibootcamp.databinding.ActivityExcercise02Binding;

public class Excercise02Activity extends AppCompatActivity {

    private ActivityExcercise02Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityExcercise02Binding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        textForgottenPassword();
        textNewAccount();
    }

    private void textForgottenPassword() {
        SpannableString string = new SpannableString("¿No recuerdas tu contraseña?");
        UnderlineSpan undrlnSpan = new UnderlineSpan();
        string.setSpan(undrlnSpan, 0, string.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        binding.textForgottenPass.setText(string);
    }

    private void textNewAccount() {
        SpannableString string = new SpannableString("Crea tu nueva cuenta");
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.MAGENTA);
        UnderlineSpan underlineSpan = new UnderlineSpan();
        string.setSpan(colorSpan, 0, string.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        string.setSpan(underlineSpan, 0, string.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        binding.textNewAccountSecond.setText(string);
    }

    public void showMessageLogin(View view) {
        Toast.makeText(this, "A Login!", Toast.LENGTH_SHORT).show();
    }

    public void showMessageForgottenMessage(View view) {
        Toast.makeText(this, "No recuerdas tu contraseña?", Toast.LENGTH_SHORT).show();
    }

    public void showMessageNewAccount(View view) {
        Toast.makeText(this, "Crea tu cuenta!", Toast.LENGTH_SHORT).show();
    }
}
