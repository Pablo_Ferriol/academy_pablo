package es.rudo.androiduibootcamp.entities;

public class User {
    private int id;
    private String username;
    private String email;
    private Boolean followed;
    private int avatar;


    public User(String username, String email, Boolean followed, int avatar) {
        this.username = username;
        this.email = email;
        this.followed = followed;
        this.avatar = avatar;
    }

    public User(String username, String email, Boolean followed) {
        this.username = username;
        this.email = email;
        this.followed = followed;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getFollowed() {
        return followed;
    }

    public void setFollowed(Boolean followed) {
        this.followed = followed;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }
}
