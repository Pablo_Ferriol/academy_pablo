package es.rudo.androiduibootcamp.Exercise07;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import es.rudo.androiduibootcamp.R;
import es.rudo.androiduibootcamp.databinding.ActivityExercise07Binding;
import es.rudo.androiduibootcamp.databinding.Fragment1Binding;

public class Exercise07Activity extends AppCompatActivity {
    private ActivityExercise07Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityExercise07Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        BottomNavigationView bottomNav = binding.bnvNavBar;
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        Fragment defaultFragment = new Fragment1();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, defaultFragment).commit();
    }

    BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment selectedFragment = null;
            switch (menuItem.getItemId()) {
                case R.id.nav_home:
                    selectedFragment = new Fragment1();
                    break;
                case R.id.nav_search:
                    selectedFragment = new Fragment2();
                    break;
                case R.id.nav_add:
                    selectedFragment = new Fragment3();
                    break;
                case R.id.nav_profile:
                    selectedFragment = new Fragment4();
                    break;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
            return true;
        }
    };
}
