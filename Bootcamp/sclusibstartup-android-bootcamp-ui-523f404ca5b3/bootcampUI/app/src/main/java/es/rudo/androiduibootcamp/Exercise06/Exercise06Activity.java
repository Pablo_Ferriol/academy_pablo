package es.rudo.androiduibootcamp.Exercise06;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import es.rudo.androiduibootcamp.databinding.ActivityExercise06Binding;

public class Exercise06Activity extends AppCompatActivity {

    private ActivityExercise06Binding binding;
    private SharedPreferences sharedPreferences;
    private String sharedFiles = "es.rudo.androiduibootcamp.Data";
    private SharedPreferences.Editor editor;

    private final String Text = "PREF_TEXT";
    private final String Check = "PREF_CHECK";
    private final String Switch = "PREF_SWITCH";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        binding = ActivityExercise06Binding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        setContentView(binding.getRoot());

        sharedPreferences = getSharedPreferences(sharedFiles, Context.MODE_PRIVATE);
        chargeDataCache();
    }

    public void saveDataCache(View view) {
        editor = sharedPreferences.edit();
        editor.putString(Text, binding.etEx6EditText.getText().toString());
        editor.putBoolean(Check, binding.cbEx6CheckBox.isChecked());
        editor.putBoolean(Switch, binding.swEx6Switch.isChecked());
        editor.apply();
    }

    private void chargeDataCache() {
        String text = sharedPreferences.getString(Text, null);
        boolean check = sharedPreferences.getBoolean(Check, false);
        boolean isSwitch = sharedPreferences.getBoolean(Switch, false);
        binding.etEx6EditText.setText(text);
        binding.cbEx6CheckBox.setChecked(check);
        binding.swEx6Switch.setChecked(isSwitch);
    }

    public void cleanDataCache(View view) {
        binding.etEx6EditText.setText("");
        binding.cbEx6CheckBox.setChecked(false);
        binding.swEx6Switch.setChecked(false);
        editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}
