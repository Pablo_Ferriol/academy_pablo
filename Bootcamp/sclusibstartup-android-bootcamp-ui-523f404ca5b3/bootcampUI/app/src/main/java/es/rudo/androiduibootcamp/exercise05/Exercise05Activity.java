package es.rudo.androiduibootcamp.exercise05;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.Toast;

import java.util.LinkedList;

import es.rudo.androiduibootcamp.R;
import es.rudo.androiduibootcamp.adapters.Exercise05Adapter;
import es.rudo.androiduibootcamp.databinding.ActivityExercise05Binding;
import es.rudo.androiduibootcamp.entities.User;

public class Exercise05Activity extends AppCompatActivity implements Exercise05Adapter.RecyclerViewClickInterface {
    private ActivityExercise05Binding binding;
    private LinkedList<User> users;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityExercise05Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        //TODO,improve code
        //region List Users
        User user0 = new User("Pablo", "Pablo@gmail.com", true, R.drawable.equipo1);
        User user1 = new User("Jesus", "Jesus@gmail.com", true, R.drawable.equipo1);
        User user2 = new User("Juan", "Juan@gmail.com", true, R.drawable.equipo2);
        User user3 = new User("Rafa", "Rafa@gmail.com", false, R.drawable.equipo1);
        User user4 = new User("Damia", "Damia@gmail.com", true, R.drawable.equipo3);
        User user5 = new User("Pablo", "Pablo@gmail.com", false, R.drawable.equipo2);
        User user6 = new User("Jesus", "Jesus@gmail.com", true, R.drawable.equipo1);
        User user7 = new User("Juan", "Juan@gmail.com", false, R.drawable.equipo3);
        User user8 = new User("Rafa", "Rafa@gmail.com", true, R.drawable.equipo1);
        User user9 = new User("Damia", "Damia@gmail.com", false, R.drawable.equipo2);
        User user10 = new User("Pablo", "Pablo@gmail.com", true, R.drawable.equipo1);
        User user11 = new User("Jesus", "Jesus@gmail.com", false, R.drawable.equipo3);
        User user12 = new User("Juan", "Juan@gmail.com", true, R.drawable.equipo2);
        User user13 = new User("Rafa", "Rafa@gmail.com", false, R.drawable.equipo1);
        User user14 = new User("Damia", "Damia@gmail.com", false, R.drawable.equipo1);
        users = new LinkedList<>();
        users.add(user0);
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        users.add(user5);
        users.add(user6);
        users.add(user7);
        users.add(user8);
        users.add(user9);
        users.add(user10);
        users.add(user11);
        users.add(user12);
        users.add(user13);
        users.add(user14);
        //endregion

        Exercise05Adapter adapter = new Exercise05Adapter(users, this);
        binding.recyclerUsers.setAdapter(adapter);
        binding.recyclerUsers.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onItemClick(int position) {
        Toast.makeText(this, users.get(position).getEmail(), Toast.LENGTH_SHORT).show();
    }
}

