package es.rudo.androiduibootcamp.adapters;


import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.LinkedList;

import es.rudo.androiduibootcamp.R;
import es.rudo.androiduibootcamp.entities.User;


public class Exercise05Adapter extends RecyclerView.Adapter<Exercise05Adapter.ViewHolder> {

    LinkedList<User> users;
    private RecyclerViewClickInterface recyclerViewClickInterface;

    //Interface
    public interface RecyclerViewClickInterface {
        void onItemClick(int position);
    }

    public Exercise05Adapter(LinkedList<User> users, RecyclerViewClickInterface recyclerViewClickInterface) {
        this.users = users;
        this.recyclerViewClickInterface = recyclerViewClickInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();//change to viewBinding?
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_search_user, viewGroup, false);
        return new ViewHolder(view);
    }

    private void changeButtonFollow(Button button) {
        button.setText("Siguiendo");
        button.setBackgroundColor(Color.parseColor("#B9B8B8"));
    }

    private void changeButtonUnFollow(Button button) {
        button.setText("Seguir");
        button.setBackgroundColor(Color.parseColor("#E91E1E"));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        String name = users.get(i).getUsername();
        int url = users.get(i).getAvatar();
        viewHolder.txtView.setText(name);
        Picasso.get().load(url).into(viewHolder.imgView);
        if (users.get(i).getFollowed()) {
            changeButtonFollow(viewHolder.btnView);
        }
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtView;
        private Button btnView;
        private ImageView imgView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView); //change to viewBinding?
            txtView = itemView.findViewById(R.id.text_username);
            btnView = itemView.findViewById(R.id.button_follow);
            imgView = itemView.findViewById(R.id.image_user);

            btnView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (btnView.getText().equals("Seguir")) {
                        changeButtonFollow(btnView);
                        users.get(getAdapterPosition()).setFollowed(true);
                    } else {
                        changeButtonUnFollow(btnView);
                        users.get(getAdapterPosition()).setFollowed(false);
                    }
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recyclerViewClickInterface.onItemClick(getAdapterPosition());
                }
            });

        }
    }
}
