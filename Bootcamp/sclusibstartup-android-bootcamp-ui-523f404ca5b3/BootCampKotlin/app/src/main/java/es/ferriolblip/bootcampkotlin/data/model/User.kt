package es.ferriolblip.bootcampkotlin.data.model

import java.io.Serializable

class User : Serializable {
    val id: Int? = 0
    var username: String?
    var email: String?
    var followed: Boolean?
    var avatar: Int? = 0

    constructor(username: String, email: String, followed: Boolean, avatar: Int) {
        this.username = username
        this.email = email
        this.followed = followed
        this.avatar = avatar
    }

    constructor(username: String, email: String, followed: Boolean) {
        this.username = username
        this.email = email
        this.followed = followed
    }
}
