package es.ferriolblip.bootcampkotlin.modules.shared

import android.content.SharedPreferences
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.ferriolblip.bootcampkotlin.helpers.Constants

class SharedViewModel : ViewModel() {
    val editText = MutableLiveData<String>()
    val checkBox = MutableLiveData<Boolean>()
    val fragSwitch = MutableLiveData<Boolean>()
/*    val shared = MutableLiveData<SharedPreferences>()*/
/*
    private val Text: String = Constants.PREF_TEXT
    private val Check: String = Constants.PREF_CHECK
    private val Switch: String = Constants.PREF_SWITCH*/

/*    fun saveData() {
        val editor = shared?.value?.edit()
        editor?.putString(Text, editText.value)
        checkBox.value?.let { editor?.putBoolean(Check, it) }
        fragSwitch.value?.let { editor?.putBoolean(Check, it) }
        editor?.apply()
    }

    fun chargeDataCache() {
        val text: String? = this.shared.value?.getString(Text, null)
        val check: Boolean? = this.shared.value?.getBoolean(Check, false)
        val isSwitch: Boolean? = this.shared.value?.getBoolean(Switch, false)
        editText.value = text
        checkBox.value = check
        fragSwitch.value = isSwitch
    }*/
}
