package es.ferriolblip.bootcampkotlin.modules.shared

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import es.ferriolblip.bootcampkotlin.R
import es.ferriolblip.bootcampkotlin.databinding.FragmentSharedBinding
import es.ferriolblip.bootcampkotlin.helpers.Constants

class SharedFragment : Fragment() {
    lateinit var binding: FragmentSharedBinding
    lateinit var viewModel: SharedViewModel

    private val Text: String = Constants.PREF_TEXT
    private val Check: String = Constants.PREF_CHECK
    private val Switch: String = Constants.PREF_SWITCH

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_shared, container, false)
        viewModel = ViewModelProvider(this).get(SharedViewModel::class.java)
        binding.lifecycleOwner = this
        binding.sharedViewModel = viewModel
        binding.sharedFragment = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
/*        viewModel.shared.value = this.activity?.getSharedPreferences("Shared", Context.MODE_PRIVATE)*/
        chargeDataCache()
    }

    fun saveDataCache() {
        val shared: SharedPreferences? =
            context?.getSharedPreferences("Shared", Context.MODE_PRIVATE)
        val editor = shared?.edit()
        editor?.putString(Text, binding.etEx6EditText.text.toString())
        editor?.putBoolean(Check, binding.cbEx6CheckBox.isChecked)
        editor?.putBoolean(Switch, binding.swEx6Switch.isChecked)
        editor?.apply()
    }

    private fun chargeDataCache() {
        val shared: SharedPreferences? =
            context?.getSharedPreferences("Shared", Context.MODE_PRIVATE)
        val text: String? = shared?.getString(Text, null)
        val check: Boolean? = shared?.getBoolean(Check, false)
        val isSwitch: Boolean? = shared?.getBoolean(Switch, false)
        viewModel.editText.value = text
        viewModel.checkBox.value = check
        viewModel.fragSwitch.value = isSwitch
    }

    fun cleanDataCache() {
        val shared: SharedPreferences? =
            context?.getSharedPreferences("Shared", Context.MODE_PRIVATE)
        viewModel.editText.value = ""
        viewModel.checkBox.value = false
        viewModel.fragSwitch.value = false
        val editor = shared?.edit()
        editor?.clear()
        editor?.apply()
    }
}