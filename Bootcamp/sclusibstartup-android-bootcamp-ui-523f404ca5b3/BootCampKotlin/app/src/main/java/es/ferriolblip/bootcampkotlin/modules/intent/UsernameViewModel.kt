package es.ferriolblip.bootcampkotlin.modules.intent

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class UsernameViewModel : ViewModel() {

    val username = MutableLiveData<String>()
    val usernameError = MutableLiveData<String>()
    val eventCheckIsOk = MutableLiveData<Boolean>()
    
    fun check() {
        var error = false
        username.value.let {
            if (it.isNullOrEmpty()) {
                usernameError.value = "Campos vacíos"
                error = true
            }
        }
/*        if (username.value.isNullOrEmpty()) {
            usernameError.value = "Campos vacíos"
            error = true
        }*/
        if (!error) eventCheckIsOk.value = true
    }

}