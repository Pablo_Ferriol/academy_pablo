package es.ferriolblip.bootcampkotlin.modules.intent

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import es.ferriolblip.bootcampkotlin.R
import es.ferriolblip.bootcampkotlin.databinding.FragmentUsernameBinding


class UsernameFragment : Fragment() {

    private lateinit var binding: FragmentUsernameBinding
    private lateinit var viewModel: UsernameViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_username, container, false)
        viewModel = ViewModelProvider(this).get(UsernameViewModel::class.java)
        binding.lifecycleOwner = this
        binding.usernameViewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObserver()
    }

    fun initObserver() {
        viewModel.usernameError.observe(viewLifecycleOwner, Observer {
            binding.editName.error = it
        })
        viewModel.eventCheckIsOk.observe(viewLifecycleOwner, Observer {
            if (it) {
                Toast.makeText(context, "Check ok", Toast.LENGTH_SHORT).show()
                sendValue()
            }
        })
    }

    fun sendValue() {
        val intent = Intent(context, ShowusernameActivity::class.java)
        intent.putExtra("INTENT_NAME", viewModel.username.value.toString())
        startActivity(intent)
    }
}