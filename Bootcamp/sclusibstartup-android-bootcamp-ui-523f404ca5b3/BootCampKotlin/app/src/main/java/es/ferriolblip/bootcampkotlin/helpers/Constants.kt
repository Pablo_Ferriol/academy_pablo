package es.ferriolblip.bootcampkotlin.helpers

object Constants {
    //Checks
    val EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    val PASSWORD_LENGTH = 8
    val USERNAME_LENGTH = 2
    val PASSWORD_PATTERN = "([A-Z])+([a-z])+([0-9])+"
    //Splas screen
    var SPLASH_TIME_OUT = 3000

    //Keys shared
    const val PREF_TEXT = "PREF_TEXT"
    const val PREF_CHECK = "PREF_CHECK"
    const val PREF_SWITCH = "PREF_SWITCH"
}