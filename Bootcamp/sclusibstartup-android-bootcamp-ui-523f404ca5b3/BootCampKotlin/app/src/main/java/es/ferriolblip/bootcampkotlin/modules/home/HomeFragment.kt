package es.ferriolblip.bootcampkotlin.modules.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import es.ferriolblip.bootcampkotlin.R
import es.ferriolblip.bootcampkotlin.adapter.HomeAdapter
import es.ferriolblip.bootcampkotlin.data.model.User
import es.ferriolblip.bootcampkotlin.databinding.FragmentHomeBinding
import kotlinx.android.synthetic.main.activity_login.*
import java.util.*

class HomeFragment : Fragment(), HomeAdapter.UserClickListener {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var viewModel: HomeViewModel
    private lateinit var users: List<User>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        binding.lifecycleOwner = this
        binding.homeViewBinding = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycle()
    }

    fun initRecycle() {
        binding.recyclerUsers.layoutManager = LinearLayoutManager(context)
        viewModel.listUsers.value?.let { users = it }
        val adapter = HomeAdapter(users, this)
        binding.recyclerUsers.adapter = adapter
    }

    override fun onClick(item: User) {
        Toast.makeText(context, item.email, Toast.LENGTH_SHORT).show()
    }
}