package es.ferriolblip.bootcampkotlin.modules.intent

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import es.ferriolblip.bootcampkotlin.R
import es.ferriolblip.bootcampkotlin.databinding.ActivityShowusernameBinding

class ShowusernameActivity : AppCompatActivity() {

    private lateinit var binding: ActivityShowusernameBinding
    private lateinit var viewModel: ShowUsernameViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_showusername)
        viewModel = ViewModelProvider(this).get(ShowUsernameViewModel::class.java)
        binding.lifecycleOwner = this
        binding.showUsernameViewModel = viewModel
        binding.showUsernameActivity = this
        getData()
    }

    private fun getData() {
        val intent = getIntent()
        val username = intent?.getStringExtra("INTENT_NAME")
        viewModel.username.value = username
    }

    fun closeApp() {
        Toast.makeText(this, "Cerrandooo!", Toast.LENGTH_SHORT).show()
        finishAffinity()
    }
}
