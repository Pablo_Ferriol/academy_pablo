package es.ferriolblip.bootcampkotlin.modules.splashscreen

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import es.ferriolblip.bootcampkotlin.R
import es.ferriolblip.bootcampkotlin.databinding.ActivitySplashBinding
import es.ferriolblip.bootcampkotlin.helpers.Constants
import es.ferriolblip.bootcampkotlin.modules.login.LoginActivity

class SplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)
        splash()
    }

    private fun splash() {
        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }, Constants.SPLASH_TIME_OUT.toLong())
    }
}