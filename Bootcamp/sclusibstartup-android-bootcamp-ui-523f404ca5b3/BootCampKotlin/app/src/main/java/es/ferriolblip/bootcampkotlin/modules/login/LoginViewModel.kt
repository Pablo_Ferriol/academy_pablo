package es.ferriolblip.bootcampkotlin.modules.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.ferriolblip.bootcampkotlin.helpers.Constants

class LoginViewModel : ViewModel() {

    //liveData
    val email = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    val emailError = MutableLiveData<String>()
    val passwordError = MutableLiveData<String>()
    var eventIsOk = MutableLiveData<Boolean>()

    //Logic
    fun check() {
        var error = false
        val textUsername: String? = email.value
        val textPassword: String? = password.value

        //emailError.value = textUsername.isNullOrEmpty().let { "Campos vacíos" }
        if (textUsername.isNullOrEmpty()) {
            emailError.value = "Campos vacíos"
            error = true
        } else {
            emailError.value = ""
        }

        textUsername?.let {
            if (!it.matches(Constants.EMAIL_PATTERN.toRegex())) {
                emailError.value = "Formato incorrecto"
                error = true
            } else {
                emailError.value = ""
            }
        }

        if (textPassword.isNullOrEmpty()) {
            emailError.value = "Campos vacíos"
            error = true
        } else {
            emailError.value = ""
        }

        if (!error) eventIsOk.value = true
    }
}