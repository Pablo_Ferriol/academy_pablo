package es.ferriolblip.bootcampkotlin.modules.maps

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import es.ferriolblip.bootcampkotlin.R
import es.ferriolblip.bootcampkotlin.databinding.FragmentMapsBinding

class MapsFragment : Fragment() {
    private lateinit var binding: FragmentMapsBinding
    private lateinit var viewModel: MapsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_maps, container, false)
        viewModel = ViewModelProvider(this).get(MapsViewModel::class.java)
        val supportMapFragment: SupportMapFragment? =
            childFragmentManager.findFragmentById(R.id.frame_maps) as SupportMapFragment?
        confMaps(supportMapFragment)
        return binding.root
    }

    private fun confMaps(supportMapFragment: SupportMapFragment?) {
        supportMapFragment?.getMapAsync { googleMap ->
            //default poss and zoom in Spain
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(39.40, -1.75), 5.0f))
            googleMap.setOnMapClickListener { latLng -> //Click on map, marker options
                val markerOptions = MarkerOptions()
                markerOptions.position(latLng)
                markerOptions.title(latLng.latitude.toString() + ":" + latLng.longitude) //set lat y long
                //show poss
                val poss = latLng.latitude.toString() + " : " + latLng.longitude
                Toast.makeText(context, poss, Toast.LENGTH_SHORT).show()
                //clean markers
                googleMap.clear()
                //animating zoom to marker
                googleMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        latLng, 10f
                    )
                )
                //add marker with config options
                googleMap.addMarker(markerOptions)
            }
        }
    }
}