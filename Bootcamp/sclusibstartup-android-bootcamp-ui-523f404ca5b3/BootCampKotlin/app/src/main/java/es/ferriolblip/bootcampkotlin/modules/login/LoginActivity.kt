package es.ferriolblip.bootcampkotlin.modules.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import es.ferriolblip.bootcampkotlin.R
import es.ferriolblip.bootcampkotlin.databinding.ActivityLoginBinding
import es.ferriolblip.bootcampkotlin.modules.navigate.NavigateActivity
import es.ferriolblip.bootcampkotlin.modules.register.RegisterActivity
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        binding.lifecycleOwner = this
        binding.loginViewModel = viewModel
        binding.loginActivity = this

        initObservers()
    }

    fun initObservers() {
        viewModel.emailError.observe(this, Observer { error ->
            input_email.error = error
        })

        viewModel.passwordError.observe(this, Observer { error ->
            input_password.error = error
        })

        viewModel.eventIsOk.observe(this, Observer<Boolean> {
            if (it) {
                Toast.makeText(this, "Login Ok!", Toast.LENGTH_LONG).show()
                goToNavigateActivity()
            } else {
                Toast.makeText(this, "Introduce datos!", Toast.LENGTH_LONG).show()
            }
        })
    }

    fun showMessageForgottenMessage() {
        Toast.makeText(this, "No recuerdas tu contraseña?", Toast.LENGTH_SHORT).show()
    }

    private fun goToNavigateActivity() {
        val intent = Intent(this, NavigateActivity::class.java)
        startActivity(intent)
    }

    fun goToRegisterActivity() {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }
}