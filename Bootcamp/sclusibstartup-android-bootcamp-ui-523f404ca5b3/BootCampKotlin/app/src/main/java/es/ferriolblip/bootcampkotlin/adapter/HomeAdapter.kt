package es.ferriolblip.bootcampkotlin.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import es.ferriolblip.bootcampkotlin.R
import es.ferriolblip.bootcampkotlin.data.model.User
import es.ferriolblip.bootcampkotlin.databinding.ItemSearchUserBinding

class HomeAdapter(
    private val users: List<User>,
    private val userClickListener: UserClickListener
) :
    RecyclerView.Adapter<HomeAdapter.HomeHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return HomeHolder(layoutInflater.inflate(R.layout.item_search_user, parent, false))
        /*  val binding = ItemSearchUserBinding.inflate(layoutInflater, parent, false)
          return ViewHolder(binding)*/
    }

    override fun onBindViewHolder(holder: HomeHolder, position: Int) {
        holder.bind(users[position], userClickListener)
    }

    override fun getItemCount(): Int {
        return users.size
    }

    class HomeHolder(val view: View) :
        RecyclerView.ViewHolder(view) {
        val binding = ItemSearchUserBinding.bind(view)
        fun bind(item: User?, userClickListener: UserClickListener) {
            binding.textUsername.text = item?.username
            // binding.clickListener = userClickListener
            item?.avatar?.let { url -> Picasso.get().load(url).into(binding.imageUser) }
            showButton(item)
            item?.let { clickListener(userClickListener, it) }
        }

        private fun showButton(item: User?) {
            if (item?.followed != true) {
                binding.buttonFollow.isVisible = true
                binding.buttonFollowed.isVisible = false
            } else {
                binding.buttonFollow.isVisible = false
                binding.buttonFollowed.isVisible = true
            }
        }

        private fun clickListener(
            userClickListener: UserClickListener,
            item: User
        ) {
            view.setOnClickListener {
                userClickListener.onClick(item)
            }
        }
    }

    interface UserClickListener {
        fun onClick(item: User)
    }
}

