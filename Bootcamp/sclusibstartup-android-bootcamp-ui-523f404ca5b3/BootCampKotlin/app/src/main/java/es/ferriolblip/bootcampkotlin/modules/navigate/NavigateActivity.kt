package es.ferriolblip.bootcampkotlin.modules.navigate

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import es.ferriolblip.bootcampkotlin.R
import es.ferriolblip.bootcampkotlin.databinding.ActivityNavigateBinding
import es.ferriolblip.bootcampkotlin.modules.home.HomeFragment
import es.ferriolblip.bootcampkotlin.modules.shared.SharedFragment
import es.ferriolblip.bootcampkotlin.modules.intent.UsernameFragment
import es.ferriolblip.bootcampkotlin.modules.maps.MapsFragment

class NavigateActivity : AppCompatActivity() {

    private lateinit var binding: ActivityNavigateBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_navigate)

        val bottomNav = binding.bnvNavBar
        bottomNav.setOnNavigationItemSelectedListener(navListener)
        val defaultFragment: Fragment = HomeFragment()
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, defaultFragment)
            .commit()
    }

    private var navListener =
        BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
            var selectedFragment: Fragment? = null
            when (menuItem.itemId) {
                R.id.nav_home -> selectedFragment = HomeFragment()
                R.id.nav_name -> selectedFragment = UsernameFragment()
                R.id.nav_shared -> selectedFragment = SharedFragment()
                R.id.nav_maps -> selectedFragment = MapsFragment()
            }
            selectedFragment?.let {
                supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    it
                ).commit()
            }
            true
        }
}