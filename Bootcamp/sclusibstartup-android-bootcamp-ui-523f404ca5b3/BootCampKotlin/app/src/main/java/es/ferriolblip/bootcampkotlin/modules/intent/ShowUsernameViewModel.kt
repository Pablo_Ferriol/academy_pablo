package es.ferriolblip.bootcampkotlin.modules.intent

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ShowUsernameViewModel : ViewModel() {
    val username = MutableLiveData<String>()
}


