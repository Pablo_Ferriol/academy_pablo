package es.ferriolblip.bootcampkotlin.modules.register

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.ferriolblip.bootcampkotlin.helpers.Constants

class RegisterViewModel : ViewModel() {

    //liveData
    val username = MutableLiveData<String>()
    val email = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    val confirPassword = MutableLiveData<String>()
    val usernameError = MutableLiveData<String>()
    val emailError = MutableLiveData<String>()
    val passwordError = MutableLiveData<String>()
    val confirmPasswordError = MutableLiveData<String>()
    val terms = MutableLiveData<Boolean>()
    val showTastTerm = MutableLiveData<Boolean>()
    var eventRegisterError = MutableLiveData<Boolean>()
    var eventIsOk = MutableLiveData<Boolean>()

    private fun checkName() {
        val username: String? = username.value
        if (username.isNullOrEmpty()) {
            usernameError.value = "Campos vacíos"
            eventRegisterError.value = true
        } else if (username.length < Constants.USERNAME_LENGTH) {
            usernameError.value = "Al menos dos caracteres"
            eventRegisterError.value = true
        }
    }

    private fun checkEmail() {
        val email: String? = email.value
        if (email.isNullOrEmpty()) {
            emailError.value = "Campos vacíos"
            eventRegisterError.value = true
        } else if (!email.matches(Constants.EMAIL_PATTERN.toRegex())) {
            emailError.value = "Formato incorrecto"
            eventRegisterError.value = true
        }
/*        email.value?.let {
            if (it.isBlank()) {
                emailError.value = "Campos vacíos"
                eventRegisterError.value = true
            } else if (!it.matches(Constants.EMAIL_PATTERN.toRegex())) {
                emailError.value = "Formato incorrecto"
                eventRegisterError.value = true
            }
        }*/
    }

    private fun checkPassword() {
        val password: String? = password.value
        if (password.isNullOrEmpty()) {
            passwordError.value = "Campos vacios"
            eventRegisterError.value = true
        } else if (!password.matches(Constants.PASSWORD_PATTERN.toRegex())) {
            passwordError.value =
                "Tiene que contener mayus número y minuscula"
            eventRegisterError.value = true
        } else if (password.length < Constants.PASSWORD_LENGTH) {
            passwordError.value = "Al menos 8 carácteres"
            eventRegisterError.value = true
        }
    }

    private fun checkConfirm() {
        val password: String? = password.value
        val confirmPass = confirPassword.value
        if (confirmPass.isNullOrEmpty()) {
            confirmPasswordError.value = "Campos vacíos"
            eventRegisterError.value = true
        } else if (confirmPass != password) {
            confirmPasswordError.value = "No coincide"
            eventRegisterError.value = true
        }
    }

    private fun checkTerms() {
        val term: Boolean? = terms.value
        if (term != true) {
            showTastTerm.value = true
            eventRegisterError.value = true
        }
    }

    fun check() {
        checkName()
        checkEmail()
        checkPassword()
        checkConfirm()
        checkTerms()
        if (eventRegisterError.value != true) eventIsOk.value = true
    }
}
