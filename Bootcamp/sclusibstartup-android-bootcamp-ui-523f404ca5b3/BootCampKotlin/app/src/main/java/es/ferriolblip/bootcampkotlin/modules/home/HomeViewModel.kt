package es.ferriolblip.bootcampkotlin.modules.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.ferriolblip.bootcampkotlin.R
import es.ferriolblip.bootcampkotlin.data.model.User

class HomeViewModel : ViewModel() {
    val listUsers = MutableLiveData<List<User>>()
    private val users : List<User>?
    init {
        users = listOf(
            User("Pablo", "Pablo@gmail.com", true, R.drawable.equipo1),
            User("Jesus", "Jesus@gmail.com", true, R.drawable.equipo1),
            User("Rafa", "Rafa@gmail.com", false, R.drawable.equipo1),
            User("Damia", "Damia@gmail.com", true, R.drawable.equipo3),
            User("Pablo", "Pablo@gmail.com", false, R.drawable.equipo2),
            User("Jesus", "Jesus@gmail.com", true, R.drawable.equipo1),
            User("Juan", "Juan@gmail.com", false, R.drawable.equipo3),
            User("Rafa", "Rafa@gmail.com", true, R.drawable.equipo1),
            User("Damia", "Damia@gmail.com", false, R.drawable.equipo2),
            User("Pablo", "Pablo@gmail.com", true, R.drawable.equipo1),
            User("Jesus", "Jesus@gmail.com", false, R.drawable.equipo3),
            User("Juan", "Juan@gmail.com", true, R.drawable.equipo2),
            User("Rafa", "Rafa@gmail.com", false, R.drawable.equipo1),
            User("Damia", "Damia@gmail.com", false, R.drawable.equipo1)
        )
        listUsers.value = users
    }
}